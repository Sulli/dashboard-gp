#!/bin/bash

cd $1

for f in *.pdf
do
	qpdf --decrypt $f 'tmp_'$f
	mv -f 'tmp_'$f $f
	rm -rf 'tmp_'$f
done