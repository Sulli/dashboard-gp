<?php
defined('_JEXEC') or die('Acces interdit');

require_once (JPATH_COMPONENT.DS.'controller.php');

if( $controller = JRequest::getWord('controller') ) 
{
	$path = JPATH_COMPONENT.DS.'controllers'.DS.$controller.'.php';

	if ( file_exists( $path ) ) 
	{
		require_once $path;
	} else 
	{
		$controller = '';
	}
}

//http://127.0.0.1/DGP/DGP/web/app_dev.php/dashboardgp
$url 		= JURI::base().'redirect_dgp.php';
JRequest::setVar( 'baseUrl' , $url );

$classname = 'DgpController'.ucfirst($controller);
$controller = new $classname( );
$controller->execute(JRequest::getCmd('task'));
$controller->redirect();

?>
