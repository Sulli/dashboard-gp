<?php
jimport( 'joomla.application.component.view');

class dgpViewManager extends JView {
	
	function display($tpl = null) {
		$mainframe = JFactory::getApplication();

		// personnaliser votre page (votre JDocument)
		$document =& JFactory::getDocument();
		$document->setTitle( "Dashboard GP" );

		$my =JFactory::getUser() ;
		$id_user = $my->id;
		
	 	$url = JRequest::getVar( 'baseUrl' );

		$this->assignRef( 'geurl', $url );

		parent::display($tpl);
	}

	function encodeId( $id )
	{
    	$r1 = rand(266,345);
    	$r2 = rand(1,9);
    	$encId = ( $r1.''.( $id + $r1 ) * $r2).''.$r2;
    	return base64_encode( $encId );
	}
}
?>
