<?php
defined('_JEXEC') or die('Accès interdit');

jimport('joomla.application.component.controller');

class DgpController extends JController 
{
	function __construct() 
	{
		parent::__construct();
	}

	function display() 
	{
		$user 			= JFactory::getUser();
		
		JRequest::setVar('view' , 		'manager' );
		JRequest::setVar('layout' ,		'dgp' );

		parent::display();
	}	
}
?>