$(document).ready(function ()
{
	$(".not-found").click(function ()
	{
		if ($(this).hasClass("file"))
		{
			$("#error-modal .media-body").empty();
			$("#error-modal .media-body").append("Fichier non trouvé.");
		}
		else if ($(this).hasClass("cv-dyb"))
		{
			$("#error-modal .media-body").empty();
			$("#error-modal .media-body").append("Vous n'avez pas de CV sur DoYouBuzz.");
		}
	});
});