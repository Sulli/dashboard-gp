$(document).ready(function () {
console.log('delays.js');
	var months = [ "Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Décembre" ];
	var NotifEnum = { SUCCESS: 0, INFO: 1, WARNING: 2 }

	updateAlerts();

	/*
	** Updates the selected index of the select control and updates the message boxes using data from the newly selected index
	*/
	$("option").click(function(){

		$("option[id='currentDelays']").removeAttr('selected');
		$("option[id='currentDelays']").removeAttr('id');

		$(this).attr('selected', 'selected');
		$(this).attr('id', 'currentDelays');

		$('#notif-alert').hide();
		$('#delays-alert').hide();

		updateAlerts();
	});

	/*
	** Converts a date string to a javascript Date object
	** Args:
	**		date: the date to convert
	*/
	function convertDate(date)
	{
		var split = date.split("-");
		var ret = new Date();

		ret.setFullYear(parseInt(split[0]), parseInt(split[1]) - 1, parseInt(split[2]));
		return ret;
	}

	/*
	** Sets the date to  1 month ago
	** Args:
	**		date: the date to decrement
	*/
	function oneMonthAgo(date)
	{
		if (date.getMonth() == 0)
		{
			date.setFullYear(date.getFullYear() - 1);
			date.setMonth(11);
		}
		else
			date.setMonth(date.getMonth() - 1);
		return date;
	}

	/*
	** Checks whether the day of the delay is before, equal or after the current day
	** Args:
	**		date: date of the delay
	**		day: comparison day
	*/
	function checkDayOfMonth(date, day)
	{
		if (date.getDate() < day)
			addNotification(date.getMonth(), date.getFullYear(), NotifEnum.INFO);
			//console.log("Don't forget to submit for " + months[nowDate.getMonth()][0] + " " + nowDate.getFullYear()); // Reminder
		else if (date.getDate() == day)
			addNotification(date.getMonth(), date.getFullYear(), NotifEnum.WARNING);
			//console.log("Submit today for " + months[nowDate.getMonth()][0] + " " + nowDate.getFullYear()); //Reminder for today
		else
			addDelay(months[date.getMonth()] + ' ' + date.getFullYear());
			//console.log("Late for " + months[nowDate.getMonth()][0] + " " + nowDate.getFullYear()) // You're late			
	}

	/*
	** Adds a delay to the delays alert box
	** Args:
	**		txt: the text to display
	*/
	function addDelay(txt)
	{
		var li = document.createElement("li");
		li.appendChild(document.createTextNode(txt));
			
		$("#delays-alert ul").append(li);
		$("#delays-alert").show();
	}

	/*
	** Sets the content of the message boxes
	** Args:
	**		month: month of the delay
	**		year: year of the delay
	**		type: type of message
	*/
	function addNotification(month, year, type)
	{
		if (type == NotifEnum.SUCCESS)
		{
			$("#notif-alert .alert").addClass("alert-success");
			$("#notif-alert .alert .fa").addClass("fa-check-circle");
			$("#notif-alert span").append("Rien à signaler.");
		}
		else
		{
			if (type == NotifEnum.INFO)
			{
				$("#notif-alert .alert").addClass("alert-info");
				$("#notif-alert .alert .fa").addClass("fa-info-circle");
			}
			else if (type == NotifEnum.WARNING)
			{			
				$("#notif-alert .alert").addClass("alert-warning");
				$("#notif-alert .fa").addClass("fa-warning");
			}
			$("#notif-alert span").append("N'a pas encore déposé son décompte pour " + months[month] + " " + year + ".");
		}
		$("#notif-alert").show();
	}

	/*
	** Processes the delays recursively
	** Args:
	**		cmpDate: original date of the delay
	**		nowDate: current date
	**		isDelayed: bool to indicate if the delay was submitted late
	**		recLvl: indicates the level of recursion
	*/
	function getAlertsRec(cmpDate, nowDate, isDelayed, recLvl)
	{
		var nowMonth = nowDate.getMonth();
		var nowYear = nowDate.getFullYear();
		var cmpMonth = cmpDate.getMonth();
		var cmpYear = cmpDate.getFullYear();

		// If there is a submission for the current month
		if (nowMonth == cmpMonth && nowYear == cmpYear)
		{
			// If it applies to this month (date_decompte FROM decompte)
			if (parseInt($('#currentDelays').attr('data-month')) == nowMonth + 1)
			{
				if (isDelayed == true || cmpDate.getDate() > 25) // If we're after the 25th
					addDelay(months[nowMonth] + ' ' + nowYear);
				return ;
			}
			else // If it applies to a previous month
				isDelayed = true;
		}

		// If there is no submission for the current month yet
		if (recLvl === 0) // If this is the first recursion (current month)
			checkDayOfMonth(nowDate, 25);
		else // Or if this isn't the first recursion (i.e, we're checking back in time)
			addDelay(months[nowMonth] + ' ' + nowYear);
		
		nowDate = oneMonthAgo(nowDate);

		if (isDelayed == true)
			cmpDate = oneMonthAgo(cmpDate);

		getAlertsRec(cmpDate, nowDate, isDelayed, recLvl + 1); // Go back in time, 1 month earlier and try to match submission
	}

	function removeClasses(elem, classes)
	{
		$.each(classes, function (idx, val) {
			if (elem.hasClass(val))
				elem.removeClass(val);
		});
	}

	/*
	** Resets the message boxes
	*/
	function cleanAlerts()
	{
		$("#delays-alert ul").empty();
		$("#notif-alert span").empty();

		removeClasses($("#notif-alert .alert"), [ "alert-info", "alert-warning", "alert-success" ]);
		removeClasses($("#notif-alert .alert .fa"), [ "fa-info-circle", "fa-warning", "fa-check-circle" ]);
	}

	/*
	** Calculates the delays and sets the message boxes accordingly
	*/
	function updateAlerts()
	{
		if ($('#currentDelays').length)
		{
			// Reset alert boxes
			cleanAlerts();

			// Calculates the delays and sets the alert boxes accordingly
			getAlertsRec(new Date($('#currentDelays').attr('data-date')), new Date());

			// If there are more than 3 alerts: only keep the last one
			if ($('#delays-alert ul li').length > 3)
			{
				date = new Date($('#currentDelays').attr('data-date'));
				$('#delays-alert ul li').remove();
				addDelay('Pas de soumission depuis ' + months[date.getMonth()] + ' ' + date.getFullYear());
			}

			// If there are no alerts: everything's fine
			if ($("#notif-alert span").text().length == 0 && $("#delays-alert ul li").length == 0)
				addNotification(null, null, NotifEnum.SUCCESS);
		}
		else
			addNotification(null, null, NotifEnum.SUCCESS);
	}
        
});