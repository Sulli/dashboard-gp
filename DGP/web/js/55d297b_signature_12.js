/*
 * THE EMAIL SIGNATURE GENERATION
 */


$(document).ready(function() {
console.log("dfgdf");
$("#signature1").hide();
$("#signature2").hide();
$("#signature1").slideUp();
$("#signature2").slideUp();


$("#sign_submit").on('click',function()
{
    /*
     * This table gets a new push at every validation process
     * The value is false if the html input is empty
     * The value is true if the input is not empty
     * @type Array
     */
    var generation = [];
    
    
    /*
     * We go throught every html input to check whether they are empty or not.
     * If they are empty, we add a css class including a red background and border
     * If not, we remove the red backgorund class that could come from a previous submit
     */
    if($('#sign_firstName').val() === "")
    {
        $('#sign_firstName').addClass('sign_background_obligatoire');
        generation.push(false);
    }
    else
    {
        $('#sign_firstName').removeClass('sign_background_obligatoire');
        generation.push(true);
    }
    
    if($('#sign_lastName').val() === "")
    {
        $('#sign_lastName').addClass('sign_background_obligatoire');
        generation.push(false);
    }
    else
    {
        $('#sign_lastName').removeClass('sign_background_obligatoire');
        generation.push(true);
    }
    
    if($('#sign_title').val() === "")
    {
        $('#sign_title').addClass('sign_background_obligatoire');
        generation.push(false);
    }
    else
    {
        $('#sign_title').removeClass('sign_background_obligatoire');
        generation.push(true);
    }
    
    if($('#sign_telephone').val() === "" && $('#sign_mobile').val() === "")
    {
        if($('#sign_telephone').val() === "")
        {
            $('#sign_telephone').addClass('sign_background_obligatoire');    
            generation.push(false);
        }
        else if($('#sign_mobile').val() === "")
        {
            $('#sign_mobile').addClass('sign_background_obligatoire');    
            generation.push(false);
        }
    }
    else
    {
        $('#sign_telephone').removeClass('sign_background_obligatoire');
        $('#sign_mobile').removeClass('sign_background_obligatoire');
        generation.push(true);
    }
    
    if($('#sign_company').val() === "")
    {
        $('#sign_company').addClass('sign_background_obligatoire');
        generation.push(false);
    }
    else
    {
        $('#sign_company').removeClass('sign_background_obligatoire');
        generation.push(true);
    }
    
    if($('#sign_address').val() === "")
    {
        $('#address').addClass('sign_background_obligatoire');
        generation.push(false);
    }
    else
    {
        $('#sign_address').removeClass('sign_background_obligatoire');
        generation.push(true);
    }
    
    if($('#sign_address').val() === "")
    {
        $('#sign_address').addClass('sign_background_obligatoire');
        generation.push(false);
    }
    else
    {
        $('#signaddress').removeClass('sign_background_obligatoire');
        generation.push(true);
    }
    
    if($('#sign_code').val() === "")
    {
        $('#sign_code').addClass('sign_background_obligatoire');
        generation.push(false);
    }
    else
    {
        $('#sign_code').removeClass('sign_background_obligatoire');
        generation.push(true);
    }
    
    if($('#sign_city').val() === "")
    {
        $('#sign_city').addClass('sign_background_obligatoire');
        generation.push(false);
    }
    else
    {
        $('#sign_city').removeClass('sign_background_obligatoire');
        generation.push(true);
    }
    
    if($('#sign_country').val() === "")
    {
        $('#sign_country').addClass('sign_background_obligatoire');
        generation.push(false);
    }
    else
    {
        $('#sign_country').removeClass('sign_background_obligatoire');
        generation.push(true);
    }
    
    /*
     * We check whether the value false is inside the array ToGenerate
     * @type @exp;generation@call;indexOf
     */
    var toGenerate = generation.indexOf(false);
    
    //console.log(generation);
    
    /*
     * If there is no false value in the toGenerate array, we hide the prevously shown signature
     * and we go to the generate function
     */
    if(toGenerate === -1)
    {
        if($("#signature1").is(':visible'))
        {
            $("#signature1").slideUp("fast",function(){
                generate();
            });
        }
        else if($("#signature2").is(':visible'))
        {
            $("#signature2").slideUp("fast",function(){
                generate();
            });
        }
        else
            generate();
    }
    else
    {
        alert("Merci de remplir les champs marqués d'une * et au moins un numéro de téléphone");
    }
    
});   


/*
 * Generates the html values and selects the signature to use (with mobile or not)
 * @returns {undefined}
 */
function generate()
{

    var firstName = $('#sign_firstName').val().charAt(0).toUpperCase()+ $('#sign_firstName').val().slice(1);

    var lastName = $('#sign_lastName').val().toUpperCase();
    var title = $('#sign_title').val().charAt(0).toUpperCase()+ $('#sign_title').val().slice(1);
    var company = $('#sign_company').val();
    var telephone = $('#sign_telephone').val();
    var mobile = $('#sign_mobile').val();
    var address = $('#sign_address').val();
    var code = $('#sign_code').val();
    var city = code + ' '+ $('#sign_city').val().charAt(0).toUpperCase()+ $('#sign_city').val().slice(1);
    var country = $('#sign_country').val().toUpperCase();

    /*
     * Will contain the signature to use
     * @type Number|Number
     */
    var signToUse;
    
    /*
     * Generates the html value in the right signature
     */
    if(mobile === "")
    {
        signToUse = 1;
        $('#sign_sign1_firstName').text(firstName);
        $('#sign_sign1_lastName').text(lastName);
        $('#sign_sign1_title').text(title);
        $('#sign_sign1_company').text(company);
        $('#sign_sign1_telephone').text(telephone);
        $('#sign_sign1_address').text(address);
        $('#sign_sign1_code').text(code);
        $('#sign_sign1_city').text(city);
        $('#sign_sign1_country').text(country);

        $("#signature1").slideDown('fast');
        $("#sign_html-block-inside").text($("#signature1").html());
        $("#sign_submit").val("Modifier");
    }
    else
    {
        signToUse = 2;
        $('#sign_sign2_firstName').text(firstName);
        $('#sign_sign2_lastName').text(lastName);
        $('#sign_sign2_title').text(title);
        $('#sign_sign2_company').text(company);
        $('#sign_sign2_telephone').text(telephone);
        $('#sign_sign2_mobile').text(mobile);
        $('#sign_sign2_address').text(address);
        $('#sign_sign2_code').text(code);
        $('#sign_sign2_city').text(city);
        $('#sign_sign2_country').text(country);
        
        $("#signature2").slideDown('fast');
        $("#sign_html-block-inside").text($("#signature2").html());
        $("#sign_submit").val("Modifier");
    }
}

});
$(document).ready(function() {
// Specify where the Flash movie can be found if not in root folder for web site
        ZeroClipboard.config( { moviePath: pathToClipboard } );
        var client = new ZeroClipboard( $("#signCopyButton") );
        var textToCopy = $('#sign_html-block-inside').val();
        client.on( "copy", function (event) {
          var textToCopy = $('#sign_html-block-inside').val();
          event.clipboardData.clearData();
          event.clipboardData.setData('text/plain',textToCopy);
          //console.log(event.clipboardData.data);
        });
});