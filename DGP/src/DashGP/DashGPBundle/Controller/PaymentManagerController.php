<?php
namespace DashGP\DashGPBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use DashGP\DashGPBundle\Entity\User;
use DashGP\DashGPBundle\Form\ManageFilesType;
use DashGP\DashGPBundle\Entity\PayformExtract;
use DashGP\DashGPBundle\Entity\MonthlyPayform;


class PaymentManagerController extends Controller
{
	/*
	** Uploads PDF files
	*/
	public function uploadIndexAction(Request $request)
	{
        $messages = array();
        $recap    = "";

        $em       = $this->getDoctrine()->getManager();
		$userId   = $this->get('dashgp.encoder')->decode( $request->get('uid') );

		$user 	  = new User( $this->container, $userId );
		$eosId    = $user->getEosId();

		if($user == null ||$user->getGpId() == null || $user->getManagerId() == null)
        {
            return $this->render('DashGPDashGPBundle:Error:invalid-id-profile.html.twig');
        }

        if( strtoupper( trim( $user->getEosEmail() ) ) != strtoupper( trim( $user->getExtranetEmail() ) ) ){
            return $this->render('DashGPDashGPBundle:Error:non-matching-profile.html.twig');
        }

        if( $user->getProfileId() != "gp" ){
            return $this->render('DashGPDashGPBundle:Error:access-refused-profile.html.twig');
        }

        $form       = $this->createForm( new ManageFilesType() );
        $formView   = $form->createView();
        $formView->children['files']->vars['full_name'] = 'collection[files][]';

        if ($request->isMethod("POST"))
        {
            $form->bind( $request );

            if( $form->isValid() ){
                $extrRepository = $em->getRepository('DashGPDashGPBundle:PayformExtract');
                $data           = $form->getData();
                $dateArr        = explode( '-', $data['datePay'] );

                $uploads        = array();
                $branches       = array();
                $anchor         = 'BP-'.$dateArr[0].'.'.$dateArr[1].'.pdf';

                foreach ( $data['files'] as $fileUpload )
                {
                    $original   = trim( $fileUpload->getClientOriginalName() );
                    $name       = str_replace( array( " -", "- ", " - ", " " ), '-', $original );
                    $name       = str_replace( '&', '-AND-', $name );

                    if( substr( $name, -14,14 ) != $anchor ){
                        /** Check if the filename follows format. If not,skip and report it. **/
                        $messages[] = array("error", $original." : Fichier non-conforme au standar.");
                        continue;
                    }

                    $branch     = str_replace( "-".$anchor, '', $name );
                    $branch     = strtoupper( str_replace( array('é','è','ê'), 'e', $branch ) );
                    $branch     = strtoupper( str_replace( array('&'), '-AND-', $branch ) );

                    if( in_array( $branch, $branches ) ){
                        $branches[] = $branch;
                    }else{
                        $branches[] = $branch;
                    }

                    $uploads[]  = $fileUpload;
                }

                $extracts = $extrRepository->getExtractsByMonthAndYear( (int) $dateArr[0], (int) $dateArr[1] );

                /** 
                * if there are extracts for the month who are not deleted or cancelled,
                * process them by type:
                * * IF they are PENDING - SET status as CANCELLED (NOT TO BE PROCESSED) - delete uploaded file
                * * IF they are PROCESSED - SET status as TODELETE (NOT TO BE PROCESSED) & related payrolls 
                *   as REPLACED - delete uploaded file
                */
                if( count( $extracts ) > 0 ){
                    $oldSave = false;

                    foreach ( $extracts as $extract ) 
                    {
                        // Check if extract is part of the uploaded documents determined branches
                        if( in_array( $extract->getBranch() , $branches ) )
                        {
                            $oldExtr = false;
                            if( $extract->getStatus() == PayformExtract::STATUS_PROCESSED ){
                                /** SET STATUS **/
                                $extract->setStatus( PayformExtract::STATUS_TODELETE );

                                /** SET STATUS of extracted payforms **/
                                if( count( $extract->getPayforms() ) > 0 ){
                                    foreach ( $extract->getPayforms() as $payform ) {
                                        $payform->setUploadStatus( MonthlyPayform::STATUS_REPLACED );
                                    }
                                }
                                $oldExtr = true;
                            }elseif( $extract->getStatus() == PayformExtract::STATUS_PENDING ){
                                /** SET STATUS **/
                                $extract->setStatus( PayformExtract::STATUS_CANCELLED );
                                $oldExtr = true;
                            }

                            // DELETE extract file if exists
                            if( is_file( $extract->getFilePath() ) ){
                                unlink( $extract->getFilePath() );
                            }

                            if( $oldExtr ){
                                $oldSave = true;
                                $em->persist( $extract );
                            }
                        }
                    }

                    if( $oldSave ){
                        $em->flush();
                    }
                }

                /** Save new extract **/
                /** For each uploaded file: 
                    * Rename it                    
                    * Put it inthe TO-BE-PROCESSED Folder
                    * Create entry as "PENDING" (To be processed in next cron update);
                **/
                $saveExtracts = false;
                $total = 0;
                foreach ( $uploads as $k => $fileUpload ) 
                {
                    $total++;
                    $original   = trim( $fileUpload->getClientOriginalName() );
                    $name       = str_replace( array( " -", "- ", " - ", " " ), '-', $original );
                    $name       = str_replace( '&', '-AND-', $name );
                    $branch     = str_replace( "-".$anchor, '', $name );
                    $branch     = strtoupper( str_replace( array('é','è','ê'), 'e', $branch ) );

                    // Move file to new path
                    $path = $this->container->getParameter('payforms').'/'.$data['datePay'];
                    if( file_exists( $path.'/'.$name ) ){
                        unlink( $path.'/'.$name );
                    }

                    $fileUpload->move( $path, $name );

                    $newExtract = new PayformExtract();

                    $newExtract->setBranch( $branch );
                    $newExtract->setMonth( (int) $dateArr[0] );
                    $newExtract->setYear( (int) $dateArr[1] );
                    $newExtract->setDateSubmitted( new \Datetime() );
                    $newExtract->setFilePath( $path.'/'.$name );
                    $newExtract->setStatus( PayformExtract::STATUS_PENDING );
                    $newExtract->setCreatedBy( $user->getId() );

                    $em->persist( $newExtract );
                    $saveExtracts = true;
                    $messages[] = array("success", $original." : Fichier téléchargé.");
                }

                if( count( $uploads ) == 0 ){
                    $messages[] = array("notice", "Aucun fichier soumis au téléchargement.");
                }

                if( $saveExtracts ){
                    $em->flush();
                }

                if( (int)date('H') < 8 ){
                    $hour = 8;
                    $recap = $total." fichiers téléchargés. Ils seront procésées à ".$hour.":00";
                }elseif ( (int)date('H') > 20 ){
                    $hour = 24;
                    $recap = $total." fichiers téléchargés. Ils seront procésées à ".$hour.":00";
                }else{
                    $cronHours = array(8,10,12,14,16,18,20);
                    $hour =  in_array( (int)date('H'), $cronHours )? (int)date('H')+2 : (int)date('H')+1;
                    $recap = $total." fichiers téléchargés.";
                    if( $total > 0 ){
                        $recap = $recap."Ils seront procésées à ".$hour.":00";
                    }
                }
            }else{
                $messages[] = array("notice", "Forme Invalide");
            }
        }

        return $this->render('DashGPDashGPBundle:Dashboard:upload.html.twig', 
                                array(  'form'      => $formView,
                                        'messages'  => $messages,
                                        'recap'     => $recap ) );
	}
       
}