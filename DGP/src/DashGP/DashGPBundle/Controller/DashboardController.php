<?php
namespace DashGP\DashGPBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use DashGP\DashGPBundle\Entity\User;
use DashGP\DashGPBundle\Entity\GenericManager;
use DashGP\DashGPBundle\Entity\Mission;
use DashGP\DashGPBundle\Form\FilesType;
use DashGP\DashGPBundle\Entity\Collection;
use DashGP\DashGPBundle\Form\CollectionType;
use DashGP\DashGPBundle\Entity\PayformExtract;
use DashGP\DashGPBundle\Entity\MonthlyPayform;

class DashboardController extends Controller
{
	public function indexAction(Request $request)
	{
		try
		{	
			$em 	= $this->getDoctrine()->getManager();		
			$userId = $this->get('dashgp.encoder')->decode( $request->get('uid') );

			$user 	= new User( $this->container, $userId );
			$eosId 	= $user->getEosId();

			if($user == null ||$user->getGpId() == null || $user->getManagerId() == null)
	        {
	        	return $this->render('DashGPDashGPBundle:Error:invalid-id-profile.html.twig');
	        }

	        if( strtoupper( trim( $user->getEosEmail() ) ) != strtoupper( trim( $user->getExtranetEmail() ) ) ){
	        	return $this->render('DashGPDashGPBundle:Error:non-matching-profile.html.twig');
	        }

	        $mgr 	= new GenericManager( $this->container, $user->getManagerId() );
            $gp 	= new GenericManager( $this->container, $user->getGpId() );

            $mission =  new Mission( $this->container, $user->getEosId() );

            //Checks if the user's contract is more than 3 months old and if it's ending soon
            $startEndDate 	= $this->get('dashgp.eos_db')->getStartEndDate( $eosId );
            $startDate 		= explode( '-' , $startEndDate['start_date'] );
            $endDate 		= $startEndDate['end_date'];

            $startDateTimeStamp = strtotime( $startDate[2].'-'.$startDate[1].'-'.$startDate[0] );
            $three_months 	= strtotime("now -3 month");
            $two_months 	= strtotime("now -2 month");
            $one_months 	= strtotime("now -1 month");

            if( $startDateTimeStamp < $three_months ){
            	$user->setStartStatus( 'in_time' );
            }else{
            	$user->setStartStatus( 'too_early' );
            }

            $user->setEndStatus( $endDate );

            /**  Retrieve Months to show **/
            $total = 1;

            $payProc 	= array();
            $payProcY1 	= array();
	        while ( $total <= 3 ) {
	            $proc   = date( 'm-Y', strtotime( "-".$total." month" ) );
	            $str 	= explode( '-' , $proc );
	            $payProc[ ((int) $str[0]).'-'.$str[1] ] = NULL;
	            $total++;
	        }
	        
	        $proc 	= date('12-Y', strtotime( "-1 year" ));
	        $str 	= explode( '-' , $proc );
	        if( !array_key_exists( ((int) $str[0]).'-'.$str[1] , $payProc ) ){
	        	$payProcY1[ ((int) $str[0]).'-'.$str[1] ] = NULL;
	        }

            $repMonthly = $em->getRepository('DashGPDashGPBundle:MonthlyPayform');
            $payforms 	= $repMonthly->findUserPayforms( $user->getEosId() );

            foreach ( $payforms as $payform ) {
            	$key = $payform->getMonth().'-'.$payform->getYear();
            	// Add to the current years payforms
            	if( array_key_exists( $key , $payProc ) and $payProc[$key] === NULL ){
            		$payProc[$key] = $payform;
            	}
            	// Add to the last years payforms
            	if( array_key_exists( $key , $payProcY1 ) and $payProcY1[$key] === NULL ){
            		$payProcY1[$key] = $payform;
            	}
            }

            foreach ($payProc as $k => $proc) {
            	if( $proc === NULL ) {
            		$dt = explode( '-', $k);
            		$mnPay = new MonthlyPayform();
					$mnPay->setMonth( $dt[0] );
					$mnPay->setYear( $dt[1] );
					$mnPay->setFilePath( "no-file" );
					$mnPay->setTitle( "no-file" );
					$mnPay->setExtension( "pdf" );
					$mnPay->setUploadStatus( MonthlyPayform::STATUS_ACTIVE );
					$mnPay->setStatus(false);
					$mnPay->setCreatedAt( new \Datetime() );
					$mnPay->setUpdatedAt( new \Datetime() );
					$payProc[ $k ] = $mnPay;
            	}
            }

            foreach ($payProcY1 as $proc) {
            	if( $proc === NULL ) {

            	}
            }

            foreach ( $payProc as $payform ) {
            	$user->addPayforms( $payform );
            }

            foreach ( $payProcY1 as $payform ) {
            	$user->addPayformsLastDecember( $payform );
            }

            $request->getSession()->set('dashgp_user', $user);

            $this->setUserContractStatus( $request );
            $this->checkDocuments( $user->getBalances() );
            $this->checkDocuments( $user->getEvaluations() );
            $this->checkPayformDocuments( $user->getPayforms() );
            $this->checkPayformDocuments( $user->getPayformsLastDecember() );
            $this->formatDelaysConsultantsNames( $request );

            $consultantsNamesAndIds = $this->get('dashgp.davidson_abs_db')->getConsultantsNamesAndIds( $userId );

            foreach ( $consultantsNamesAndIds as $i => $consultantElm ) 
            {
            	$idCslt 		= $consultantElm['id_user'];
                $csltEosId 		= $this->get('dashgp.davidson_abs_db')->getEosId( $idCslt );

                $csltPersonInfo = $this->get('dashgp.eos_db')->getUserPersonInfos( $csltEosId );
                $csltFirstName 	= ucfirst( $csltPersonInfo['first_name'] );
                $csltLastName 	= strtoupper( $csltPersonInfo['last_name'] );
                $consultantElm['nom_consultant'] = $csltFirstName." ".$csltLastName;

                $delaysByCslt 	= $this->get('dashgp.davidson_abs_db')->getConsultantDelays($idCslt);
                $consultantElm['delays'] = $delaysByCslt;
            }

            $consultantsDelays = $consultantsNamesAndIds;

            $dueDay 		= 25;
            $actualYear 	= intval( date( "Y" ) );
            $actualMonth 	= intval( date( "m" ) );
            $actualDay 		= intval( date( "d" ) );

            $previousMonth 	= intval( date( "m", strtotime( "-1 Month" ) ) );
            $previousYear 	= intval( date( "Y", strtotime( "-1 Month" ) ) );

            foreach ( $consultantsDelays as $key => $csltDelay ) 
            {
            	$cd = $csltDelay['delays'][0];

				if( $actualDay < $dueDay )
				{
					/** Si on est avant le 25 au mois 2, 
						il faut valider le mois 1 sinon on est en retard **/
					if( $cd['date_decompte'] == $previousMonth ){
						$csltDelay['actualdelay'] = 'nodelay';
					}elseif( $cd['date_decompte'] == $actualMonth ){
						$csltDelay['actualdelay'] = 'nodelay';
					}else{
						$csltDelay['actualdelay'] = 'actualdelay';
					}
				}
				else
				{
					//si on est apres le 25 au mois 2, il faut valider le mois 2
					if($cd['date_decompte'] == $actualMonth){
						$csltDelay['actualdelay'] = 'nodelay';
					}else{
						$csltDelay['actualdelay'] = 'actualdelay';
					}
				}

				if( $endDate != null )
                {
                    $endDate 		= explode( '-',$startEndDate['end_date'] );
                    $endDateYear 	= intval( $endDate[0] );
                    $endDateMonth 	= intval( $endDate[1] );
                    $endDateDay 	= intval( $endDate[2] );

                    $endDateTmstp 	= mktime(0, 0, 0, $endDateMonth , $endDateDay, $endDateYear );
                    $OneMonTmstp 	= 2678400;

                    if( $endDateTmstp + $OneMonTmstp < time() )
                        $csltDelay['out'] = 'out';
                }
                else
                {
                    $csltDelay['out'] = 'in';
                }  
            }
        }catch ( Exception $e )
		{
			$message = "L'application a rencontrée une erreur.</p><p>Veuillez vous reporter au service informatique.";
			$params = array( 'message' => $message );
			return $this->render('DashGPDashGPBundle:Error:500-error.html.twig', $params );
		}
                
        if( !isset( $idConsultant ) )
        {
            $idConsultant = null;
        }

        $actualTimeStamp 	= time();

        if( $mission->EndDate() != null )
        {
        	$endDateArr = explode( '-',$mission->EndDate() );
        	$endDateLMTimestamp = mktime( 0, 0, 0, $endDateArr[1], $endDateArr[2], $endDateArr[0] );

        	if( $$endDateLMTimestamp > $actualTimeStamp){
        		$missionToShow = 'yes';
        	}else{
        		$missionToShow = 'no';
        	}
        }else{
        	$missionToShow = 'yes';
        }
                
		return $this->render('DashGPDashGPBundle:Dashboard:index.html.twig',
			array(
				'user'				=> $user,
				'mgr'				=> $mgr,
				'gp'				=> $gp,
				'mission'			=> $mission,
                'missionToShow' 	=> $missionToShow,
                'consultantsDelays' => $consultantsDelays,
                'idConsultant'      => $idConsultant
			) );
	}

	/********************\
	* Controller Actions *  
	\********************/

	/*
	** Returns the user's salary information
	*/
	public function getSalaryAction(Request $request)
	{
		if ( $request->isMethod("POST") )
		{
			$salary = $request->getSession()->get('dashgp_user')->getSalary();

			return new Response( stripslashes( json_encode( $salary ) ) );
		}
		
		return null;
	}

	/*
	** Returns an employer/insurance certification file
	** Args:
	**		$type: employer or insurance
	*/
	public function getCertificationAction(Request $request, $type)
	{
		$user = $request->getSession()->get('dashgp_user');
		$auth = $this->getParameter('eos_server');
		$content = $this->get('dashgp.helper.document')->getCertification($type, $auth, $user->getEosId());

		if ($type == 'employer')
			$filename = 'attestation_salaire_' . $user->getSlug() . '.doc';
		else if ($type == 'insurance')
			$filename = 'attestation_mutuelle_' . $user->getSlug() . '.doc';
		
		return $this->buildResponse($content, array(
			'Content-Type'	=> 'application/msword',
			'Content-Disposition'	=> 'attachment; filename=' . $filename
			));
	}

	/*
	** Returns a payform/balance/evaluation file
	** Args:
	**		$type: the type of file (payform, balance, evaluation)
	**		$idx: the index of the file in the array
	*/
	public function getDocumentAction(Request $request, $type, $idx)
	{
		$user = $request->getSession()->get('dashgp_user');

		if( $type == "payform" ){
			$type = 'get' . ucfirst($type . 's');
			$file = $user->$type()[$idx]->getPath();
			$content = $this->get('dashgp.helper.document')->getDocument($file);
		}else{
			$type = 'get' . ucfirst($type . 's');
			$file = $this->getParameter('user_docs') . '/' . $user->$type()[$idx]->getPath();
			$content = $this->get('dashgp.helper.document')->getDocument($file);
		}
		$filename = str_replace(' ', '_', $user->$type()[$idx]->getTitle() . '.' . $user->$type()[$idx]->getExtension());

		return $this->buildResponse($content, array(
			'Content-Type'			=> $user->$type()[$idx]->getMimeType(),
			'Content-Disposition'	=> 'attachment; filename=' . $filename
		));
	}

	/******************\
	* Controller Tools *
	\******************/

	/*
	** Instantiates an object dynamically
	** Args:
	**		$type: the type of the object to instantiate (User, GenericManager, Mission)
	**		$id: the id of the user/manager
	**		$profileId: the profile id of the user
	*/
	private function create($type, $id, $profileId = null)
	{
            
		if (($profileId != null) && ($profileId == 'str'))
			return null;
		return new $type($this->container, $id);
	}

	/*
	** Checks the existence of every document in a collection
	** Args:
	**		$docs: the collection of documents
	*/
	private function checkDocuments($docs)
	{
		if ($docs == null)
			return null;

		$path = $this->getParameter('user_docs');

		foreach ($docs as $doc)
			$doc->setStatus(file_exists($path . '/' . $doc->getPath()));
	}

	/*
	** Checks the existence of the payforms
	** Args:
	**		$docs: the collection of documents
	*/
	private function checkPayformDocuments($docs)
	{
		if ($docs == null)
			return null;

		foreach ($docs as $doc)
			$doc->setStatus(file_exists( $doc->getPath() ) );
	}

	/*
	** Formats the names of a manager's consultants: Firstname LASTNAME
	*/
	private function formatDelaysConsultantsNames( Request $request )
	{
		$user = $request->getSession()->get('dashgp_user');

		if ( $user->getDelays() == null ){
			return null;
		}

		foreach ( $user->getDelays() as $delay )
		{
			$consultant = $delay->getConsultant();
	
			if ( in_array( $user->getProfileId(), $this->getParameter('app_managers') ) 
					&& isset( $consultant ) && !empty( $consultant ) )
			{
				$name 	= explode('.', $consultant);
				$first 	= '';
				$last 	= '';

				if ( isset( $name[1] ) && !empty( $name[1] ) ){
					$first = ucwords(strtolower($name[1]));
				}
					
				if ( isset( $name[0] ) && !empty( $name[0] ) ){
					$last = strtoupper($name[0]);
				}

				$delay->setConsultant( $first.' '.$last );
			}
		}
	}

	/*
	** Sets a user's contract according to the user's status (out or trial period)
	*/
	private function setUserContractStatus(Request $request)
	{
		$user 	= $request->getSession()->get('dashgp_user');
		$diff 	= explode('-', date_diff(date_create(date('Y-m-d')), date_create($user->getContract()['start']))->format('%Y-%m-%d'));

		$year 	= intval($diff[0]);
		$month 	= intval($diff[1]);
		$day 	= intval($diff[2]);

		$predicate 	= ($year == 0) && ((($month == 3) && ($day == 0)) || (($month < 3) && ($day >= 0)));
		$contract 	= $user->getContract();

		if (empty($contract) || isset($contract['end']) || ($predicate == true)){
			$user->setContract(null);
		}
	}

	/*
	** Generic response builder
	** Args:
	**		$content: the content of the response
	**		$headers: the headers of the response
	*/
	private function buildResponse($content, $headers)
	{
		$response = new Response();

		foreach ($headers as $key => $value){
			$response->headers->set($key, $value);
		}

		$response->setContent($content);
		
		return $response;
	}

	/*
	** Shortcut for $this->container->getParameter($param)
	*/
	private function getParameter( $param )
	{
		return $this->container->getParameter($param);
	}
        
    public function ajaxDelaysAction()
    {
        try
        {           
            $request = $this->getRequest();

            $idConsultant 		= $request->request->get('idConsultant');
            $consultantDelays 	= $this->get('dashgp.davidson_abs_db')->getConsultantDelays($idConsultant);
            $eosId 				= $this->get('dashgp.davidson_abs_db')->getEosId($idConsultant);

            //Checks if the user's contract is ending soon
            $startEndDate 	= $this->get('dashgp.eos_db')->getStartEndDate($eosId);
            $startDate 		= explode('-',$startEndDate['start_date']);
            $endDate 		= $startEndDate['end_date'];

            $dueDay = 25;

            if(intval(date("d")) > $dueDay && intval(date("m")))
            {
                $lastDueDate 		= date("Y").'-'.date("m").'-'.$dueDay;
                $lastDueDateTmstp 	= mktime(0, 0, 0, date("m"), $dueDay, date("Y"));
            }
            else
            {
                $month1 			= date("m")-1;
                $lastDueDate 		= date("Y").'-'.$month1.'-'.$dueDay;
                $lastDueDateTmstp 	= mktime(0, 0, 0, date("m")-1, $dueDay, date("Y"));
            }

            $count = count($consultantDelays);

            for($i=0;$i < $count ;$i++)
            {
                $dateSoumission 	= $consultantDelays[$i]['date_soumission'];
                $dateSubmisExplode 	= explode('-',$dateSoumission);
                $dateSubmisYear 	= intval($dateSubmisExplode[0]);

                if($dateSubmisYear == 0){
                	$dateSubmisYear = 2013;
                }

                $dateSubmisMonth 	= intval($dateSubmisExplode[1]);
                $dateSubmisDay 		= intval($dateSubmisExplode[2]);
                $dueYear 			= $consultantDelays[$i]['annee'];

                if($dueYear == 0){
                	$dueYear = 2013;
                }

                $dueMonth 	= $consultantDelays[$i]['date_decompte'];
                $dueDate 	= $dueYear.'-'.$dueMonth.'-'.$dueDay;

                $compareDateSubmis 	= mktime(0, 0, 0, $dateSubmisMonth, $dateSubmisDay, $dateSubmisYear);
                $compareDueDate 	= mktime(0, 0, 0, $dueMonth, $dueDay, $dueYear);

                if($compareDateSubmis > $compareDueDate){
                	$consultantDelays[$i]['delay'] = 'delay';
                }else{
                	$consultantDelays[$i]['delay'] = 'no';
                }
            }

            return $this->render('DashGPDashGPBundle:Dashboard:Blocks/delays-individual.html.twig',array('consultantDelays'=> $consultantDelays));
        }
        catch (Exception $e)
        {
        	$message = "L'application a rencontrée une erreur.</p><p>Veuillez vous reporter au service informatique.";
			$params = array( 'message' => $message );
			return $this->render('DashGPDashGPBundle:Error:500-error.html.twig', $params );
        }        
    }
       
}