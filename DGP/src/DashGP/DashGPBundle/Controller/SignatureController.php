<?php
namespace DashGP\DashGPBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use DashGP\DashGPBundle\Entity\User;
use DashGP\DashGPBundle\Entity\Branch;
use DashGP\DashGPBundle\Entity\GenericManager;
use DashGP\DashGPBundle\Entity\Mission;
use DashGP\DashGPBundle\Form\FilesType;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\HttpFoundation\JsonResponse;

class SignatureController extends Controller
{
    public function indexAction($extranetId)
    {
        //http://localhost/Davidson/extranet/dgp_manager/app_dev.php/dashboardgp/%9A%CC%8A%B7%BBNz%C2%5BE%89%87%03k%B9%9B

        $decId = $extranetId;
        $r1 = substr($decId, 0, 3);
        $r2 = substr($decId, strlen($decId) - 2, 2);


        $num = substr($decId, 3, strlen($decId) - 5);
        if( $r2 == 0 )
            return new Response();
        $davId = ''. (($num / $r2) - $r1);
        
        $user = new User($this->container,$davId);
        $eosId = $user->getEosId();
        
        $branchQuery = $this->get('dashgp.eos_db')->getBranch($eosId);
        $branchQuery = $branchQuery[0];
        $branch = new Branch(
                $branchQuery['id'],
                $branchQuery['name'],
                $branchQuery['address'],
                $branchQuery['address_city'],
                $branchQuery['address_postalcode'],
                $branchQuery['address_country']);
        
        $userFirstName = $user->getFirstName();
        $userLastName = $user->getLastName();
        $matchMobile = '#^06#';
        
        if(preg_match($matchMobile , $user->getPhonePro() ) == true)
        {
            $user->setPhoneMobile($user->getPhonePro());
            $user->setPhonePro('');
        }
        if(preg_match($matchMobile , $user->getPhoneMobile() )!= true)
            $user->setPhoneMobile('');
        //die(var_dump($user->getPhonePro()));

        return $this->render('DashGPDashGPBundle:Signature:index.html.twig',array
            (
                'user'  => $user,
                'branch'=>$branch,
            ));
    }
    public function downloadAction($id)
    { 
        $request = $this->getRequest();
        
        $firstName = htmlentities($request->get('firstName'));
        $lastName = htmlentities($request->get('lastName'));
        $title = htmlentities($request->get('title'));
        $company = htmlentities($request->get('company'));
        $telephone = htmlentities($request->get('telephone'));
        $mobile = htmlentities($request->get('mobile'));
        $address = htmlentities($request->get('address'));
        $city = htmlentities($request->get('city'));
        $country = htmlentities($request->get('country'));
        $signToUse = htmlentities($request->get('signToUse'));
        $telOrMobile = htmlentities($request->get('telOrMobile'));
        

        $result = $this->get('dashgp.sign.ref')->getSignature($firstName,$lastName,$title,$company,$telephone,$mobile,$telOrMobile,$address,$city,$country,$signToUse);
        $result = $result;
        //die(var_dump($result));
        
        
        $response = new Response();
        $filename = "davidson_2015.htm";
        $fichier = "davidson_2015";
        $response->setContent($result);

        // the headers public attribute is a ResponseHeaderBag
        $response->headers->set('Content-Type', 'text/plain');
        $response->headers->set('Content-Disposition', 'attachment; filename='. $fichier.'.htm');

        return $response;
    }
}