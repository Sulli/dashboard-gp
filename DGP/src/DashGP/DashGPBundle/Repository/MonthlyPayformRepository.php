<?php

namespace DashGP\DashGPBundle\Repository;
use DashGP\DashGPBundle\Entity\MonthlyPayform;

use Doctrine\ORM\EntityRepository;

/**
 * MonthlyPayformRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class MonthlyPayformRepository extends EntityRepository
{
	public function findUserPayforms( $user_id, $uploadStatus = MonthlyPayform::STATUS_ACTIVE ){
		$query = $this->createQueryBuilder('MonthlyPayform')
						->where('MonthlyPayform.eosId = :user_id')
						->andWhere('MonthlyPayform.uploadStatus = :uploadStatus')
						->setParameter('user_id', $user_id )
						->setParameter('uploadStatus', $uploadStatus )
						->addOrderBy('MonthlyPayform.year', 'DESC')
   						->addOrderBy('MonthlyPayform.month', 'DESC')
						->getQuery();

 		return $query->getResult();		
	}
}
