<?php

namespace DashGP\DashGPBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ManageFilesType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $months = array();

        $months[1]  = "Janvier";
        $months[2]  = "Février";
        $months[3]  = "Mars";
        $months[4]  = "Avril";
        $months[5]  = "Mai";
        $months[6]  = "Juin";
        $months[7]  = "Juillet";
        $months[8]  = "Août";
        $months[9]  = "Septembre";
        $months[10] = "Octobre";
        $months[11] = "Novembre";
        $months[12] = "Décembre";

        $datesProc      = array();
        $defaultDate    = "";

        $total = 1;

        while ( $total <= 3 ) {
            $proc   = date( 'm-Y', strtotime( "-".$total." month" ) );
            $str    = explode( "-", $proc );
            if( $total == 1 ){
                $defaultDate = $proc;
            }
            $datesProc[ $proc ] = $months[ (int) $str[0] ].'-'.$str[1];
            $total++;
        }
        
        $proc = date('12-Y', strtotime( "-1 year" ));
        $str    = explode( "-", $proc );
        $datesProc[ $proc ] = $months[ (int) $str[0] ].'-'.$str[1];


        $builder->add(  'datePay', 'choice',  array(    'choices'   => $datesProc,
                                                        'label'     => 'Date : ', 
                                                        'data'      => $defaultDate, 
                                                        'multiple'  => false) );

        $builder->add( 'files', 'file', array(  "label"     => "Fichiers",
                                                "required"  => TRUE,
                                                "attr"      => array(   "accept" => "application/pdf",
                                                                        "multiple" => "multiple",
                                                                        "name" => "files[]" )
        ) );

        $builder->add('Envoyer', 'submit');
    }

    public function getName()
    {
    	return 'collection';
    }
}