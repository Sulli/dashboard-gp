<?php

namespace DashGP\DashGPBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class EmptyBlankSpacesCommand extends ContainerAwareCommand
{
	protected function configure()
	{
		$this->setName('dashgp:generate:spaces:pdf');
	}

	/*
	** Creates the payforms PDF files
	*/
	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$dirHelper = $this->getContainer()->get('dashgp.helper.directory');
		$pdfHelper = $this->getContainer()->get('dashgp.helper.pdf');

		// Sets the source folder
		$src = $dirHelper->formatPath($this->getContainer()->getParameter('payforms'));
		// Sets the destination folder
		$dest = $dirHelper->formatPath($this->getContainer()->getParameter('user_docs'));
                
        //scans all the files in the directory
		$objects = scandir($src);

		//rename the files to avoid blank spaces
		for($i=0;$i<count($objects);$i++)
		{
		    $match = preg_match('#(\d{2}.\d{4})\.pdf$#', $objects[$i], $result);

		    if(is_file($src.$objects[$i])& ($match == 1))
			{
	            $newName = str_replace(' ','',$objects[$i]);
	            rename($src.$objects[$i],$src.$newName);
			}
		}
	}
}