<?php

namespace DashGP\DashGPBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ExtractUserDocsCommand extends ContainerAwareCommand
{
	protected function configure()
	{
		$this->setName('dashgp:extract:user:docs');
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$dirHelper = $this->getContainer()->get('dashgp.helper.directory');
		$docHelper = $this->getContainer()->get('dashgp.helper.document');

		// Sets the source folder
		$src = $dirHelper->formatPath($this->getContainer()->getParameter('exports'));
		// Sets the destination folder
		$dest = $dirHelper->formatPath($this->getContainer()->getParameter('user_docs'));

		$objects = scandir($src);

		// Iterates through each tar.gz, decompresses them and moves the extracted files
		foreach ($objects as $object)
		{
			$match = preg_match('#^export_\d+\.tar\.gz$#', $object, $result);

			if ((filetype($src . $object) == 'file') && $match === 1)
			{
				$docHelper->decompress($src . $object);
				$subdir = $src . basename($object, '.tar.gz');

				foreach (['balance', 'evaluation'] as $item)
					$docHelper->moveFiles($item, $subdir, $dest, '#^.+-(.+-.+)-.+#');
			}
		}
	}
}