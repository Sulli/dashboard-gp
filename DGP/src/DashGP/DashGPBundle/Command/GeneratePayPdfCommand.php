<?php

namespace DashGP\DashGPBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use DashGP\DashGPBundle\Entity\PayformExtract;

class GeneratePayPdfCommand extends ContainerAwareCommand
{
	protected function configure()
	{
		$this->setName('dashgp:parse:pay:pdf');
	}

	/*
	** Creates the payforms PDF files
	*/
	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$em 		= $this->getContainer()->get('doctrine.orm.entity_manager');
		$dirHelper 	= $this->getContainer()->get('dashgp.helper.directory');
		$pdfHelper 	= $this->getContainer()->get('dashgp.helper.pdf');

		// Retrieve source folders
		$src  = $dirHelper->formatPath( $this->getContainer()->getParameter('payforms') );
		$dest = $dirHelper->formatPath( $this->getContainer()->getParameter('user_docs') );
		$base = $dirHelper->formatPath( $this->getContainer()->getParameter('base_docs') );

		$repository = $em->getRepository('DashGPDashGPBundle:PayformExtract');

		$extracts = $repository->findByStatus( PayformExtract::STATUS_PENDING );

		foreach ( $extracts as $extract )
		{
			$filePath = $extract->getFilePath();
			$fileBase = $src.$extract->getDatePay().'/';
			$fileName = str_replace( $fileBase, "", $filePath);

			exec( $base.'/unlock_pdf.sh '.$fileBase.' '.$fileName );

			$match = preg_match('#(\d{2}.\d{4})\.pdf$#', $fileName, $result);

			if ( ( filetype($fileBase . $fileName) == 'file') && ($match === 1))
			{
				$pdfHelper->split($fileBase . $fileName, $dest, $extract);
				//unlink($src . $object);
			}

			$extract->setStatus( PayformExtract::STATUS_PROCESSED );
			$em->persist( $extract );
		}
		$em->flush();
	}
}