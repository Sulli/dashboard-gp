<?php

namespace DashGP\DashGPBundle\Entity;

use DashGP\DashGPBundle\Entity\Person;

class GenericManager extends Person
{
	private $gsm;
	private $phone;
	private $email;
	private $avatar;

	public function __construct( $container, $eosId )
	{
		parent::__construct($container->get('dashgp.db_manager'));

		$infos 				= $this->dbMgr->getManagerInfos($eosId);
		$this->id 			= $infos["id"];
		$this->eosId 		= $infos["eosId"];
		$this->firstName 	= $infos["first_name"];
		$this->lastName 	= $infos["last_name"];
		if (isset($infos["cb_gsm"])){
			$this->gsm 	= $infos["cb_gsm"];
		}
			
		if (isset($infos["cb_telpro"])){
			$this->phone = $infos["cb_telpro"];
		}
			
		if (isset($infos["email"])){
			$this->email = $infos["email"];
		}
			
		if (isset($infos["avatar"])){
			$this->avatar = $infos["avatar"];
		}
	}

	public function Gsm()
	{
		return $this->gsm;
	}

	public function Phone()
	{
		return $this->phone;
	}

	public function Email()
	{
		return $this->email;
	}

	public function Avatar()
	{
		return $this->avatar;
	}
}