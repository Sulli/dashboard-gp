<?php

namespace DashGP\DashGPBundle\Entity;

use DashGP\DashGPBundle\Entity\Document;

/*
** Balance document
*/
class Balance extends Document
{
	/**
     * @var integer year
     */
	private $year;

	/**
     * Constructor for the yearly balances documents
     */
	public function __construct( $data )
	{
		parent::__construct( $data );

		$this->year = $data['year'];
	}

	/*
	** Serializes the instance of the class
	** Used to store it in session
	*/
	public function serialize()
	{
		return serialize(array(
			'year'		=> $this->year,
			'parent'	=> parent::serialize()
			));
	}

	/*
	** Unserializes the previously serialized instance
	** Used to retrieve it from session
	*/
	public function unserialize( $serialized )
	{
		$data = unserialize( $serialized );

		$this->year = $data['year'];
		parent::unserialize( $data['parent'] );
	}

	/**
     * Get year
     *
     * @return integer 
     */
	public function getYear()
	{
		return $this->year;
	}
}