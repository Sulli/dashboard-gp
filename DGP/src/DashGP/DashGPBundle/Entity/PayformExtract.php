<?php

namespace DashGP\DashGPBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use DashGP\DashGPBundle\Entity\MonthlyPayform;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Payform
 *
 * @ORM\Table(name="dav_payform_extract")
 * @ORM\Entity(repositoryClass="DashGP\DashGPBundle\Repository\PayformExtractRepository")
 */
class PayformExtract
{
    const STATUS_PROCESSED  = "PROCESSED";
    const STATUS_PENDING    = "PENDING";
    const STATUS_CANCELLED  = "CANCELLED";
    const STATUS_DELETED    = "DELETED";
    const STATUS_TODELETE   = "TO_DELETE";

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="branch", type="string" , length=255, nullable=false)
     */
    private $branch;

    /**
     * @var integer
     *
     * @ORM\Column(name="month", type="integer" , nullable=false)
     */
    private $month;

    /**
     * @var integer
     *
     * @ORM\Column(name="year", type="integer" , nullable=false)
     */
    private $year;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_submitted", type="datetime" , nullable=false)
     */
    private $dateSubmitted;

    /**
     * @var text
     *
     * @ORM\Column(name="file_path", type="text", nullable=false)
     */
    private $filePath;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255, nullable=false)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime" , nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime" , nullable=false)
     */
    private $updatedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="closed_at", type="datetime" , nullable=true)
     */
    private $closedAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="created_by", type="integer" , nullable=false)
     */
    private $createdBy;

    /**
     * @ORM\OneToMany(targetEntity="DashGP\DashGPBundle\Entity\MonthlyPayform", mappedBy="extract", cascade={"persist", "remove"} )
     */
    private $payforms;

    public function __construct()
    {
        $this->status       = PayformExtract::STATUS_PENDING;
        $this->payforms     = new ArrayCollection();
        $this->createdAt    = new \Datetime();
        $this->updatedAt    = new \Datetime();
    }

    /**
     * get id
     *
     * @return integer id
     */
    public function getId(){
        return $this->id;
    }
    
    /**
     * get eos ID
     *
     * @return integer eosId
     */
    public function getEosId(){
        return $this->eosId;
    }

    /**
     * Set Eos ID
     *
     * @param integer $eosId
     * @return PayformExtract
     */
    public function setEosId( $eosId ){
        $this->eosId = $eosId;
        return $this;
    }

    /**
     * get branch
     *
     * @return string branch
     */    
    public function getBranch(){
        return $this->branch;
    }

    /**
     * Set branch
     *
     * @param string $branch
     * @return PayformExtract
     */
    public function setBranch( $branch ){
        $this->branch = $branch;
        return $this;
    }

    /**
     * get month
     *
     * @return integer month
     */    
    public function getMonth(){
        return $this->month;
    }

    /**
     * Set Month
     *
     * @param integer $month
     * @return PayformExtract
     */
    public function setMonth( $month ){
        $this->month = $month;
        return $this;
    }

    /**
     * get year
     *
     * @return integer year
     */
    public function getYear(){
        return $this->year;
    }

    /**
     * Set Year
     *
     * @param integer $year
     * @return PayformExtract
     */
    public function setYear( $year ){
        $this->year = $year;
        return $this;
    }

    /**
     * get date submitted
     *
     * @return Datetime dateSubmitted
     */
    public function getDateSubmitted(){
        return $this->dateSubmitted;
    }

    /**
     * Set Date Submitted
     *
     * @param Datetime $dateSubmitted
     * @return PayformExtract
     */
    public function setDateSubmitted( $dateSubmitted ){
        $this->dateSubmitted = $dateSubmitted;
        return $this;

    }
    
    /**
     * get file Path
     *
     * @return text filePath
     */
    public function getFilePath(){
        return $this->filePath;
    }

    /**
     * Set File Path
     *
     * @param string $filePath
     * @return PayformExtract
     */
    public function setFilePath( $filePath ){
        $this->filePath = $filePath;
        return $this;
    }
    
    /**
     * get status
     *
     * @return boolean status
     */
    public function getStatus(){
        return $this->status;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return PayformExtract
     */
    public function setStatus( $status ){
        $this->status = $status;
        return $this;
    }


    /**
     * get Date created
     *
     * @return Datetime createdAt
     */
    public function getCreatedAt(){
        return $this->createdAt;
    }

    /**
     * Set Date created
     *
     * @param Datetime $createdAt
     * @return PayformExtract
     */
    public function setCreatedAt( $createdAt ){
        $this->createdAt = $createdAt;
        return $this;

    }

    /**
     * get date Updated
     *
     * @return Datetime updatedAt
     */
    public function getUpdatedAt(){
        return $this->updatedAt;
    }

    /**
     * Set Date Updated
     *
     * @param Datetime $updatedAt
     * @return PayformExtract
     */
    public function setUpdatedAt( $updatedAt ){
        $this->updatedAt = $updatedAt;
        return $this;
    }

    /**
     * get date closed
     *
     * @return Datetime closedAt
     */
    public function getClosedAt(){
        return $this->closedAt;
    }

    /**
     * Set Date closed
     *
     * @param Datetime $closedAt
     * @return PayformExtract
     */
    public function setClosedAt( $closedAt ){
        $this->closedAt = $closedAt;
        return $this;
    }

    /**
     * get createdBy
     *
     * @return integer createdBy
     */
    public function getCreatedBy(){
        return $this->createdBy;
    }

    /**
     * Set createdBy
     *
     * @param integer $createdBy
     * @return PayformExtract
     */
    public function setCreatedBy( $createdBy ){
        $this->createdBy = $createdBy;
        return $this;
    }

    /**
     * Add payform
     *
     * @param DashGP\DashGPBundle\Entity\MonthlyPayform $payform
     * @return PayformExtract
     */
    public function addPayforms(\DashGP\DashGPBundle\Entity\MonthlyPayform $payform)
    {
        $payform->setExtract( $this );
        $this->payforms[] = $payform;
    
        return $this;
    }

    /**
     * Remove payform
     *
     * @param \DashGP\DashGPBundle\Entity\MonthlyPayform $payform
     */
    public function removePayforms(\DashGP\DashGPBundle\Entity\MonthlyPayform $payform)
    {
        $this->payforms->removeElement( $payform );
    }

    /**
     * Get payforms
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPayforms()
    {
        return $this->payforms;
    }

    /***************************/
    /** File Upload Functions **/
    /***************************/
    public function setDatePay( $datePay ){
        $datePayArr     = explode( "-", $datePay );
        $this->month    = (int) $datePayArr[0];
        $this->year     = (int) $datePayArr[1];

        return $this;
    }

    public function getDatePay( ){
        $datePay = (($this->month < 10)? '0'.$this->month : $this->month ).'-'.$this->year;
        return $datePay;
    }
}