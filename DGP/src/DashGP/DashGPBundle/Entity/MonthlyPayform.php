<?php

namespace DashGP\DashGPBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use DashGP\DashGPBundle\Entity\PayformExtract;

/**
 * Payform
 *
 * @ORM\Table(name="dav_monthly_payform")
 * @ORM\Entity(repositoryClass="DashGP\DashGPBundle\Repository\MonthlyPayformRepository")
 */
class MonthlyPayform
{
    const STATUS_ACTIVE     = "ACTIVE";
    const STATUS_REPLACED   = "REPLACED";
    const STATUS_DELETED    = "DELETED";
    const STATUS_TODELETE   = "TO_DELETE";

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="eos_id", type="integer" , nullable=false)
     */
    private $eosId;

    /**
     * @var integer
     *
     * @ORM\Column(name="month", type="integer" , nullable=false)
     */
    private $month;

    /**
     * @var integer
     *
     * @ORM\Column(name="year", type="integer" , nullable=false)
     */
    private $year;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_submitted", type="datetime" , nullable=false)
     */
    private $dateSubmitted;

    /**
     * @var text
     *
     * @ORM\Column(name="file_title", type="text", nullable=false)
     */
    private $fileTitle;

    /**
     * @var text
     *
     * @ORM\Column(name="file_extension", type="text", nullable=false)
     */
    private $fileExtension;

    /**
     * @var text
     *
     * @ORM\Column(name="file_path", type="text", nullable=false)
     */
    private $filePath;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="boolean", nullable=false)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="upload_status", type="string", nullable=false)
     */
    private $uploadStatus;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime" , nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime" , nullable=false)
     */
    private $updatedAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="created_by", type="integer" , nullable=false)
     */
    private $createdBy;

    /**
     * @var PayformExtract $extract
     * @ORM\ManyToOne(targetEntity="DashGP\DashGPBundle\Entity\PayformExtract",  inversedBy="payforms", cascade={"persist", "merge"})
     * @ORM\JoinColumn(name="extract_id", nullable=true)
     */
    private $extract;

    /**
     * Get Title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->fileTitle;
    }

    /**
     * Get Extension
     *
     * @return string
     */
    public function getExtension()
    {
        return $this->fileExtension;
    }

    public function setTitle( $fileTitle )
    {
        $this->fileTitle = $fileTitle;
        return $this;
    }

    public function setExtension( $fileExtension )
    {
        $this->fileExtension = $fileExtension;
        return $this;
    }

    public function __construct()
    {
        $this->status       = true;
        $this->uploadStatus = MonthlyPayform::STATUS_ACTIVE; 
    }

    /**
     * get id
     *
     * @return integer id
     */
    public function getId(){
        return $this->id;
    }
    
    /**
     * get eos ID
     *
     * @return integer eosId
     */
    public function getEosId(){
        return $this->eosId;
    }

    /**
     * Set Eos ID
     *
     * @param integer $eosId
     * @return MonthlyPayform
     */
    public function setEosId( $eosId ){
        $this->eosId = $eosId;
        return $this;
    }

    /**
     * get month
     *
     * @return integer month
     */    
    public function getMonth(){
        return $this->month;
    }

    /**
     * Set Month
     *
     * @param integer $month
     * @return MonthlyPayform
     */
    public function setMonth( $month ){
        $this->month = $month;
        return $this;
    }

    /**
     * get year
     *
     * @return integer year
     */
    public function getYear(){
        return $this->year;
    }

    /**
     * Set Year
     *
     * @param integer $year
     * @return MonthlyPayform
     */
    public function setYear( $year ){
        $this->year = $year;
        return $this;
    }

    /**
     * get date submitted
     *
     * @return Datetime dateSubmitted
     */
    public function getDateSubmitted(){
        return $this->dateSubmitted;
    }

    /**
     * Set Date Submitted
     *
     * @param Datetime $dateSubmitted
     * @return MonthlyPayform
     */
    public function setDateSubmitted( $dateSubmitted ){
        $this->dateSubmitted = $dateSubmitted;
        return $this;

    }
    
    /**
     * get file Path
     *
     * @return text filePath
     */
    public function getFilePath(){
        return $this->filePath;
    }

    public function getPath()
    {
        return $this->filePath;
    }

    /**
     * Set File Path
     *
     * @param string $filePath
     * @return MonthlyPayform
     */
    public function setFilePath( $filePath ){
        $this->filePath = $filePath;
        return $this;
    }
    
    /**
     * get status
     *
     * @return string status
     */
    public function getStatus(){
        return $this->status;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return MonthlyPayform
     */
    public function setStatus( $status ){
        $this->status = $status;
        return $this;
    }

    /**
     * get upload status
     *
     * @return string uploadStatus
     */
    public function getUploadStatus(){
        return $this->uploadStatus;
    }

    /**
     * Set uploadStatus
     *
     * @param string $uploadStatus
     * @return MonthlyPayform
     */
    public function setUploadStatus( $uploadStatus ){
        $this->uploadStatus = $uploadStatus;
        return $this;
    }

    /**
     * get Date created
     *
     * @return Datetime createdAt
     */
    public function getCreatedAt(){
        return $this->createdAt;
    }

    /**
     * Set Date created
     *
     * @param Datetime $createdAt
     * @return MonthlyPayform
     */
    public function setCreatedAt( $createdAt ){
        $this->createdAt = $createdAt;
        return $this;

    }

    /**
     * get date Updated
     *
     * @return Datetime updatedAt
     */
    public function getUpdatedAt(){
        return $this->updatedAt;
    }

    /**
     * Set Date Updated
     *
     * @param Datetime $updatedAt
     * @return MonthlyPayform
     */
    public function setUpdatedAt( $updatedAt ){
        $this->updatedAt = $updatedAt;
        return $this;
    }

    /**
     * get createdBy
     *
     * @return integer createdBy
     */
    public function getCreatedBy(){
        return $this->createdBy;
    }

    /**
     * Set createdBy
     *
     * @param integer $createdBy
     * @return MonthlyPayform
     */
    public function setCreatedBy( $createdBy ){
        $this->createdBy = $createdBy;
        return $this;
    }

    /**
     * get extract
     *
     * @return \DashGP\DashGPBundle\Entity\PayformExtract extract
     */
    public function getExtract(){
        return $this->extract;
    }

    /**
     * Set extract
     *
     * @param \DashGP\DashGPBundle\Entity\PayformExtract $extract
     * @return MonthlyPayform
     */
    public function setExtract( \DashGP\DashGPBundle\Entity\PayformExtract $extract ){
        $this->extract = $extract;
        return $this;
    }

    public function getMimeType(){
        return "application/pdf";
    }
}