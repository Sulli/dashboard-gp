<?php

namespace DashGP\DashGPBundle\Entity;

use DashGP\DashGPBundle\Entity\Object;

class Mission extends Object
{
	
	private $clientId;
	private $clientName;
	private $clientAddress;
	private $clientContact;
	private $title;
	private $transportFee;
	private $transportFeeComment;
	private $restorationFee;
	private $restorationFeeComment;
    private $endDate;

	public function __construct($container, $eosId)
	{
		parent::__construct( $container->get('dashgp.db_manager') );

		$infos = $this->dbMgr->getMissionInfos( $eosId );

		$this->clientId = $infos["client_id"];
		$this->clientName = $infos["client_name"];
		$this->clientAddress = $infos["address"];
		$this->clientContact = $infos["client_contact"];
		$this->title = $infos["name"];
		$this->transportFee = $infos["transport_fee"];
		$this->transportFeeComment = $infos["transport_fee_comment"];
		$this->restorationFee = $infos["restoration_fee"];
		$this->restorationFeeComment = $infos["restoration_fee_comment"];
                $this->endDate = $infos["end_date"];
	}

	public function ClientId()
	{
		return $this->clientId;
	}

	public function ClientName()
	{
		return $this->clientName;
	}

	public function ClientAddress()
	{
		return $this->clientAddress;
	}

	public function ClientContact()
	{
		return $this->clientContact;
	}

	public function Title()
	{
		return $this->title;
	}

	public function TransportFee()
	{
		return $this->transportFee;
	}

	public function TransportFeeComment()
	{
		return $this->transportFeeComment;
	}

	public function RestorationFee()
	{
		return $this->restorationFee;
	}

	public function RestorationFeeComment()
	{
		return $this->restorationFeeComment;
	}
        
        public function EndDate()
	{
		return $this->endDate;
	}
}