<?php

namespace DashGP\DashGPBundle\Entity;

/**
 * Branch
 *
 */
class Branch
{
    /**
     * @var integer id
     */
    private $id;

    /**
     * @var string branch's name
     *
     */
    private $name;

    /**
     * @var string Address
     */
    private $address;

    /**
     * @var string City
     */
    private $addressCity;

    /**
     * @var integer Postal Code
     */
    private $addressPostalcode;

    /**
     * @var string Country
     */
    private $addressCountry;

    /**
     * @var string Phone Extension
     *
     */
    private $extension;

    /**
     * Build branch constructor
     *
     */
    public function __construct( $id, $name, $address, $addressCity, $addressPostalcode, $addressCountry )
    {
        $this->setId( $id );
        $this->setName( $name );
        $this->setAddress( $address );
        $this->setaddressCity( $addressCity );
        $this->setaddressPostalcode( $addressPostalcode );
        $this->setaddressCountry( $addressCountry );
        
        $addressCountry = $this->getAddressCountry();

        $extensionsNumbers =
            [
                'france'        => '+33',
                'allemagne'     => '+49',
                'germany'       => '+49',
                'belgique'      => '+32',
                'belgium'       => '+32',
                'suisse'        => '+41',
                'switzerland'   => '+41',
                'españa'        => '+34',
                'espana'        => '+34',
                'espagne'       => '+34',
                'spain'         => '+34'
            ];
        
        if( array_key_exists( strtolower( $addressCountry ),$extensionsNumbers ) === true )
        {
            $ext = $extensionsNumbers[ strtolower( $addressCountry ) ];
            $this->setExtension( $ext );
        }else{
            $this->setExtension("");
        }
    }

    /**
     * Set id
     *
     * @param integer $id
     * @return Branch
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Branch
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Branch
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set addressCity
     *
     * @param string $addressCity
     * @return Branch
     */
    public function setAddressCity($addressCity)
    {
        $this->addressCity = $addressCity;

        return $this;
    }

    /**
     * Get addressCity
     *
     * @return string 
     */
    public function getAddressCity()
    {
        return $this->addressCity;
    }

    /**
     * Set addressPostalcode
     *
     * @param integer $addressPostalcode
     * @return Branch
     */
    public function setAddressPostalcode($addressPostalcode)
    {
        $this->addressPostalcode = $addressPostalcode;

        return $this;
    }

    /**
     * Get addressPostalcode
     *
     * @return integer
     */
    public function getAddressPostalcode()
    {
        return $this->addressPostalcode;
    }

    /**
     * Set addressCountry
     *
     * @param string $addressCountry
     * @return Branch
     */
    public function setAddressCountry($addressCountry)
    {
        $this->addressCountry = $addressCountry;

        return $this;
    }

    /**
     * Get addressCountry
     *
     * @return string 
     */
    public function getAddressCountry()
    {
        return $this->addressCountry;
    }
    
    /**
     * Set extension
     *
     * @param string $extension
     * @return Branch
     */
    public function setExtension($extension)
    {
        $this->extension = $extension;

        return $this;
    }
    
    /**
     * Get extension
     *
     * @return string 
     */
    public function getExtension()
    {
        return $this->extension;
    }
}
