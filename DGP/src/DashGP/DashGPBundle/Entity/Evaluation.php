<?php

namespace DashGP\DashGPBundle\Entity;

use DashGP\DashGPBundle\Entity\Document;

/*
** Evaluation document
*/
class Evaluation extends Document
{
	private $mission;

	public function __construct($data)
	{
		parent::__construct($data);

		$this->mission = $data['name'];
	}

	/*
	** Serializes the instance of the class
	** Used to store it in session
	*/
	public function serialize()
	{
		return serialize(array(
			'mission'	=> $this->mission,
			'parent'	=> parent::serialize()
			));
	}

	/*
	** Deserializes the previously serialized instance
	** Used to retrieve it from session
	*/
	public function unserialize($serialized)
	{
		$data = unserialize($serialized);

		$this->mission = $data['mission'];
		parent::unserialize($data['parent']);
	}

	public function getMission()
	{
		return $this->mission;
	}
}