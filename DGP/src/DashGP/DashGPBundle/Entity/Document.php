<?php

namespace DashGP\DashGPBundle\Entity;

/*
** Class representing a generic document
** Used for Balance, Evaluation, Payform
*/
abstract class Document implements \Serializable
{
	/**
     * @var string
     *
     */
	protected $title;

	/**
     * @var string
     *
     */
	protected $extension;

	/**
     * @var string
     *
     */
	protected $mime_type;

	/**
     * @var text
     *
     */
	protected $path;

	/**
     * @var boolean
     *
     */
	protected $status;

	public function __construct( $data )
	{
		$this->title 		= $data['file_title'];
		$this->extension 	= $data['file_extension'];
		$this->mime_type 	= $data['file_mime_type'];
		$this->path 		= $data['file_path'];
		$this->status 		= false;
	}

	/*
	** Serializes the instance of the class
	** Used to store it in session
	*/
	public function serialize()
	{
		return serialize(array(
			'title'		=> $this->title,
			'extension'	=> $this->extension,
			'mime_type'	=> $this->mime_type,
			'path'		=> $this->path,
			'status'	=> $this->status
			));
	}

	/*
	** Deserializes the previously serialized instance
	** Used to retrieve it from session
	*/
	public function unserialize($serialized)
	{
		$data = unserialize($serialized);

		$this->title 		= $data['title'];
		$this->extension 	= $data['extension'];
		$this->mime_type 	= $data['mime_type'];
		$this->path 		= $data['path'];
		$this->status 		= $data['status'];
	}

	/**
     * Get Title
     *
     * @return string
     */
	public function getTitle()
	{
		return $this->title;
	}

	/**
     * Get Extension
     *
     * @return string
     */
	public function getExtension()
	{
		return $this->extension;
	}

	/**
     * Get Mime
     *
     * @return string
     */
	public function getMimeType()
	{
		return $this->mime_type;
	}

	/**
     * Get path
     *
     * @return text
     */
	public function getPath()
	{
		return $this->path;
	}

	/**
     * Get status
     *
     * @return boolean
     */
	public function getStatus()
	{
		return $this->status;
	}

	/**
     * Set status
     *
     * @param Boolean $value
     * @return Document
     */
	public function setStatus($value)
	{
		$this->status = $value;
	}
}