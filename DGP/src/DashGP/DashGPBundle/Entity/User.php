<?php

namespace DashGP\DashGPBundle\Entity;

use DashGP\DashGPBundle\Entity\Person;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use DashGP\DashGPBundle\Entity\Evaluation;
use DashGP\DashGPBundle\Entity\Balance;
use DashGP\DashGPBundle\Entity\Payform;

/*
** Class that represents a user
*/
class User extends Person implements \Serializable
{
	
	/**
	*	string slug 
	**/
	private $slug;
	
	/**
	*	string profileId 
	**/
	private $profileId;
	
	/**
	*	string managerId 
	**/
	private $managerId;
	
	/**
	*	string gpId 
	**/
	private $gpId;
	
	/**
	*	string salary 
	**/
	private $salary;
	
	/**
	*	string delays 
	**/
	private $delays;
	
	/**
	*	url cvUrl 
	**/
	private $cvUrl;
	
	/**
	*	string contract 
	**/
	private $contract;

	/**
	*	collection evaluations
	**/
	private $evaluations;

	/**
	*	collection balances
	**/
	private $balances;

	/**
	*	collection payforms
	**/
	private $payforms;

	/**
	*	collection payformsLastDecember
	**/
    private $payformsLastDecember;

	/**
	*	string start status
	**/
    private $startStatus;

    /**
	*	string end status
	**/
    private $endStatus;

    /**
	*	string phone professional
	**/
    private $phonePro;

    /**
	*	string mobile phone
	**/
    private $phoneMobile;

    /**
	*	string job title
	**/
    private $jobTitle;

    /**
	*	Constructor
	**/
	public function __construct( $container, $id )
	{
		parent::__construct( $container->get('dashgp.db_manager') );

		/** Retrieve User Infos **/
		$infos 		= $container->get('dashgp.db_manager')->getUserInfos( $id );
		$jos_array 	= $container->get('dashgp.extranet_db')->getInfos( $id );

		if ( $infos === NULL || $jos_array === NULL ){
			throw new AuthenticationException;
		}

		/** Person **/
		$this->id 			= $id;
		/*** EOS ***/
		$this->eosId 		= $infos['eosId'];
		$this->firstName 	= $infos['first_name'];
		$this->lastName 	= $infos['last_name'];
		$this->eosEmail 	= $infos['email'];
		/*** Extranet ***/
		$this->extranetEmail = $jos_array['email'];

		/** User **/
		/*** EOS ***/
		$this->slug 		= $infos['slug'];
		$this->phonePro 	= $infos['phone_number'];
		$this->profileId 	= $infos['profile_id'];
		$this->managerId 	= $infos['manager_id'];
		$this->gpId 		= $infos['gp_id'];
		$this->salary 		= $infos['salary'];
		$this->contract 	= $infos['contract'];
		/*** Extranet ***/
		$this->phoneMobile 	= $jos_array['cb_gsm'];
        $this->jobTitle 	= $jos_array['cb_job'];
		
		$this->cvUrl 		= $container->get('dashgp.dyb_helper')
										->getPdfCvUrl( $this->firstName, $this->lastName );

		$this->delays = NULL;
		if ( !in_array( $this->profileId, $container->getParameter('app_managers') ) )
		{
			if ( $infos['delays'] ){
				$this->delays = new Delay( $infos['delays'] );
			}
		}else{
			$this->delays = $this->toObjectCollection( $infos['delays'], 'DashGP\DashGPBundle\Entity\Delay' );
		}

		$this->evaluations 			= $this->toObjectCollection( $infos['evaluations'], 'DashGP\DashGPBundle\Entity\Evaluation' );
		$this->balances 			= $this->toObjectCollection( $infos['balances'], 	'DashGP\DashGPBundle\Entity\Balance' );
		$this->payforms 			= NULL;
		$this->payformsLastDecember = NULL;
	}

	/*
	** Initializes the payforms for the 3 previous months + december n-1
	*/
	private function createPayforms()
	{
		$payforms 	= array();
		$flag 		= false;

		for( $i = 1; $i <= 3 && ( $flag == false ); ++$i )
		{
			$date 	= date('m_Y', strtotime('-' . $i . ' month') );
			$flag 	= preg_match( '#^12_\d{4}$#', $date );
			$path 	= $this->slug.'/payforms/paie_'.$date.'_'.$this->slug.'_'.$this->eosId.'.pdf';
			
			$payforms[] = array(
				'file_title' 		=> 'paie_' . $date . '_' . $this->slug,
				'file_extension'	=> 'pdf',
				'file_mime_type'	=> 'application/pdf',
				'file_path'			=> $path,
				'date'				=> explode( '_', $date ) );
		}
                
		return $payforms;
	}

	/*
	** Initializes the payforms for december n-1
	*/
	private function createPayformsLastDecember()
	{
		$payforms 	= array();
		$flag 		= false;

		$date 		= '12_' . intval( date('Y') - 1 );
		$flag 		= preg_match( '#^12_\d{4}$#', $date );

		$path 		= $this->slug.'/payforms/paie_'.$date.'_'.$this->slug.'_'.$this->eosId.'.pdf';

		$payforms[] = array(
            'file_title'		=> 'paie_' . $date . '_' . $this->slug,
            'file_extension'	=> 'pdf',
            'file_mime_type'	=> 'application/pdf',
            'file_path'			=> $path,
            'date'				=> explode( '_', $date ) );
                
		return $payforms;
	}


	/*
	** Converts an array of arrays to an array of objects of a given type
	** Args:
	**		$array: the original array
	**		$type: the type of object
	*/
	private function toObjectCollection($array, $type)
	{
		if ( $array == null )
			return null;

		$ret = array();

		foreach ( $array as $value )
			$ret[] = new $type( $value );

		return $ret;
	}

	/*
	** Serializes the instance of the class
	** Used to store it in session
	*/
	public function serialize()
	{
		return serialize( array(
			'id'			=> $this->id,
			'eosId'			=> $this->eosId,
			'profileId'		=> $this->profileId,
			'slug'			=> $this->slug,
			'salary'		=> $this->salary,
			'evaluations' 	=> $this->evaluations,
			'balances'		=> $this->balances,
			'payforms'		=> $this->payforms,
			'delays'		=> $this->delays
			) );
	}

	/*
	** Deserializes the previously serialized instance
	** Used to retrieve it from session
	*/
	public function unserialize($serialized)
	{
		$data = unserialize($serialized);

		$this->id 			= $data['id'];
		$this->eosId 		= $data['eosId'];
		$this->profileId 	= $data['profileId'];
		$this->slug 		= $data['slug'];
		$this->salary 		= $data['salary'];
		$this->evaluations 	= $data['evaluations'];
		$this->balances 	= $data['balances'];
		$this->payforms 	= $data['payforms'];
		$this->delays 		= $data['delays'];
	}

	public function getSlug()
	{
		return $this->slug;
	}

	public function getProfileId()
	{
		return $this->profileId;
	}

	public function getManagerId()
	{
		return $this->managerId;
	}

	public function getGpId()
	{
		return $this->gpId;
	}

	public function getSalary()
	{
		return $this->salary;
	}

	public function getCvUrl()
	{
		return $this->cvUrl;
	}

	public function getContract()
	{
		return $this->contract;
	}

	public function setContract($value)
	{
		$this->contract = $value;
	}

	public function getDelays()
	{
		return $this->delays;
	}

	public function getEvaluations()
	{
		return $this->evaluations;
	}

	public function getBalances()
	{
		return $this->balances;
	}

	public function setEvaluations( $evaluations )
	{
		$this->evaluations = $evaluations;
		return $this;
	}

	public function setBalances( $balances )
	{
		$this->balances = $balances;
		return $this;
	}
        
	public function getPhonePro()
	{
		return $this->phonePro;
	}
        
	public function setPhonePro($phonePro)
	{
		$this->phonePro = $phonePro;
	}
        
	public function getPhoneMobile()
	{
		return $this->phoneMobile;
	}
        
	public function setPhoneMobile($phoneMobile)
	{
		$this->phoneMobile = $phoneMobile;
	}
        
	public function getJobTitle()
	{
		return $this->jobTitle;
	}

	public function getPayforms()
	{
		return $this->payforms;
	}
        
	public function getPayformsLastDecember()
	{
		return $this->payformsLastDecember;
	}

	public function setPayforms( $payforms )
	{
		$this->payforms = $payforms;
		return $this;
	}

	public function addPayforms( $payform )
	{
		$this->payforms[] = $payform;
		return $this;
	}
        
	public function setPayformsLastDecember( $payformsLastDecember )
	{
		$this->payformsLastDecember = $payformsLastDecember;
		return $this;
	}

	public function addPayformsLastDecember( $payformLastDecember )
	{
		$this->payformsLastDecember[] = $payformLastDecember;
		return $this;
	}
	
	public function getStartStatus()
	{
		return $this->startStatus;
	}

	public function setStartStatus($startStatus)
	{
		$this->startStatus = $startStatus;
	}
        
	public function getEndStatus()
	{
		return $this->endStatus;
	}

	public function setEndStatus($endStatus)
	{
		$this->endStatus = $endStatus;
	}
}