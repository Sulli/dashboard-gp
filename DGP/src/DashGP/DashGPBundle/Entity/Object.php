<?php

namespace DashGP\DashGPBundle\Entity;

abstract class Object
{
	/**
	*	DataBase Manager
	**/
	protected $dbMgr;

	public function __construct( $dbMgr )
	{
		$this->dbMgr = $dbMgr;
	}
}