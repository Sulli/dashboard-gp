<?php

namespace DashGP\DashGPBundle\Entity;

use DashGP\DashGPBundle\Entity\Object;

abstract class Person extends Object
{
	/**
	*	integer id => Extranet ID
	**/
	protected $id;

	/**
	*	integer id => EOS ID
	**/
	protected $eosId;

	/**
	*	string firstName => EOS First Name
	**/
	protected $firstName;

	/**
	*	string lastName => EOS LastName Name
	**/
	protected $lastName;
	
	/**
	*	string User's Eos EMAIL (main)
	**/
	protected $eosEmail;

	/**
	*	string User's Extranet EMAIL (secondary - used for validation)
	**/
	protected $extranetEmail;

	/**
     * Get Extranet ID
     *
     * @return integer id
     */
	public function getId()
	{
		return $this->id;
	}

	/**
     * Set Extranet ID
     *
     * @param integer $id
     *
     * @return Person
     */
	public function setId( $id )
	{
		$this->id = $id;
		return $this;
	}

	/**
     * Get EOS ID
     *
     * @return integer eosId
     */
	public function getEosId()
	{
		return $this->eosId;
	}

	/**
     * Set EOS ID
     *
     * @param integer $eosId
     *
     * @return Person
     */
	public function setEosId( $eosId )
	{
		$this->eosId = $eosId;

		return $this;
	}

	/**
     * Get First Name
     *
     * @return string firstName
     */
	public function getFirstName()
	{
		return $this->firstName;
	}

	/**
     * Set First Name
     *
     * @param string $firstName 
     *
     * @return Person
     */
	public function setFirstName( $firstName )
	{
		$this->firstName = $firstName;

		return $this;
	}

	/**
     * Get Last Name
     *
     * @return string lastName
     */
	public function getLastName()
	{
		return $this->lastName;
	}

	/**
     * Set Last Name
     *
     * @param string $lastName 
     *
     * @return Person
     */
	public function setLastName( $lastName )
	{
		$this->lastName = $lastName;

		return $this;
	}

	/**
     * Get Eos Email
     *
     * @return string eosEmail
     */
	public function getEosEmail()
	{
		return $this->eosEmail;
	}

	/**
     * Set Eos Email
     *
     * @param string $eosEmail 
     *
     * @return Person
     */
	public function setEosEmail( $eosEmail )
	{
		$this->eosEmail = $eosEmail;

		return $this;
	}

	/**
     * Get Extranet Email
     *
     * @return string extranetEmail
     */
	public function getExtranetEmail()
	{
		return $this->extranetEmail;
	}

	/**
     * Set Extranet Email
     *
     * @param string extranetEmail
     *
     * @return Person
     */
	public function setExtranetEmail( $extranetEmail )
	{
		$this->extranetEmail = $extranetEmail;

		return $this;
	}
}