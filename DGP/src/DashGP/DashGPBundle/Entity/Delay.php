<?php

namespace DashGP\DashGPBundle\Entity;

class Delay implements \Serializable
{
	/**
     * @var DateTime 
     */
	private $submitDate;

	/**
     * @var integer
     */
	private $month;

	/**
     * @var integer
     */
	private $year;

	/**
     * @var string
     */
	private $consultant;

	/**
     * Build Object
     */
	public function __construct($data)
	{
		$this->submitDate 	= $data['date_soumission'];
		$this->month 		= $data['date_decompte'];
		$this->year 		= $data['annee'];
		$this->consultant 	= $data['nom_consultant'];
	}

	/*
	** Serializes the instance of the class
	** Used to store it in session
	*/
	public function serialize()
	{
		return serialize(array(
			'submitDate'	=> $this->submitDate,
			'month'			=> $this->month,
			'year'			=> $this->year,
			'consultant'	=> $this->consultant
			));	
	}

	/*
	** Deserializes the previously serialized instance
	** Used to retrieve it from session
	*/
	public function unserialize($serialized)
	{
		$data = unserialize($serialized);

		$this->submitData 	= $data['submitDate'];
		$this->month 		= $data['month'];
		$this->year 		= $data['year'];
		$this->consultant 	= $data['consultant'];
	}

	/**
     * Get submit Date
     *
     * @return DateTime
     */
	public function getSubmitDate()
	{
		return $this->submitDate;
	}

	/**
     * Set submit Date
     *
     * @param dateTime submitDate
     * @return Delay
     */
	public function setSubmitDate( $submitDate )
	{
		$this->submitDate = $submitDate;
		return $this;
	}

	/**
     * Get month
     *
     * @return Integer
     */
	public function getMonth()
	{
		return $this->month;
	}

	/**
     * Set month
     *
     * @param integer $month
     * @return Delay
     */
	public function setMonth( $month )
	{
		$this->month = $month;
		return $this;
	}

	/**
     * Get Year
     *
     * @return Integer
     */
	public function getYear()
	{
		return $this->year;
	}

	/**
     * Set year
     *
     * @param integer $tearvalue
     * @return Delay
     */
	public function setYear( $year )
	{
		$this->year = $year;
		return $this;
	}

	/**
     * Get Consultant Name
     *
     * @return string
     */
	public function getConsultant()
	{
		return $this->consultant;
	}

	/**
     * Set Consultant Name
     *
     * @param string $value
     * @return Delay
     */
	public function setConsultant( $value )
	{
		$this->consultant = $value;
	}
}