$(document).ready(function ()
{
	$('#collection_files').change(function ()
	{
		$('.processed-files').each( function(){
			$( this ).hide();
		});

		$('.recap-files').hide();

		$('.list-group-item').remove();
	
		if ($('.list-group').length == 0)
			$('#file-upload .panel-body').append('<ul class="list-group"></ul>')
	
		$.each($(this).prop('files'), function ()
		{
			$('.list-group').append('<li class="list-group-item">' + $(this).prop('name') + '</li>');
		});
	});
});