function drawChart(data) {

    $('#container').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: null
        },
        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '12px',
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: null
            }
        },
        legend: {
            enabled: false
        },
        plotOptions: {
            bar: { dataLabels: { enabled: true } }
        },
        series: [{
            name: 'Salaire',
            data: data,
            pointWidth: 10,
            dataLabels: {
                align: 'left',
                enabled: true,
                rotation: -90,
                style: {
                    fontSize: '10px',
                }
            }
        }],
        exporting: { enabled: false }
    });
}

$(document).ready(function () {
    $.ajax({
        type: "POST",
        //url: "http://178.255.101.42/davidson.fr/extranet/dgp_manager/app.php/dashboardgp/salary",
        url: "salary",
        dataType: "json",
        success: function(data, dataType)
        {
            var param = [];

            $.each(data, function(key, value) {
                param.push({
                    name: "Au " + value.dateFrom,
                    y: parseFloat(value.amount),
                    color: (key == (data.length - 1)) ? '#90ed7d' : '#7cb5ec'
                });
            });
            drawChart(param);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown)
        {
            console.log('Error : ' + errorThrown);
        }
    });
});