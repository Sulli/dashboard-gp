<?php

namespace DashGP\DashGPBundle\Services;

use Zeroem\CurlBundle\Curl\Request as CurlRequest;
use Zeroem\CurlBundle\Curl\CurlErrorException;

class DoYouBuzzHelper
{
	/*
	** Returns the url to a DoYouBuzz PDF CV
	** Args:
	**		$firstname: the first name of the user to match
	**		$lastname: the last name of the user to match
	*/
	public function getPdfCvUrl($firstname, $lastname)
	{
		try
		{
			// Requests the API for all users whose last name matches $lastname
			// /users/search => http://doc.doyoubuzz.com/pro/users
			$users = $this->doCurlRequest("users/search", array("term" => $lastname));

			// Iterates through the results of the previous query
			foreach ($users as $idx => $user)
			{
				// No matches found
				if ($idx === "error"){
					return null;
				}

				// If both the first name and the last name match the result
				if (strcasecmp($user["firstname"], $firstname) == 0 && strcasecmp($user["lastname"], $lastname) == 0)
				{
					// Requests the API for the user's cv information
					// /users/:id/cv => http://doc.doyoubuzz.com/pro/users
					$cvData = $this->doCurlRequest("users/" . $user["id"] . "/cv");

					// Iterates through the results of the previous query to find the 'main' cv, and return its url
					foreach ( $cvData as $cv )
					{
						if ( $cv["main"] == 1 )
						{
							// /cv/:id/pdf => http://doc.doyoubuzz.com/pro/cv
							return $this->doCurlRequest("cv/" . $cv["id"] . "/pdf")["pdf"];
						}
					}
				}
			}
		}
		catch (CurlErrorException $e)
		{
			return null;
		}
		return null;
	}

	/*
	** Returns the result of an API request
	** Args:
	**		$method: the API method to query
	**		$params: the parameters to pass to the method
	*/
	private function doCurlRequest($method, $params = null)
	{
		if ( $params == null ){
			$params = array();
		}
		
		$params = $this->buildDoYouBuzzAPIRequestParams(array_merge($params, array(
			"apikey"	=> "uQa0n2tU7OYMqCUvQFDr",
			"timestamp"	=> time()
			)
		));
                
		$requestURL = $this->buildDoYouBuzzAPIRequest($method, $params);
		$curl 		= new CurlRequest( $requestURL );
		$curl->setOption(CURLOPT_RETURNTRANSFER, true);

		$response 	= json_decode( $curl->execute(), true );
		return $response;
	}

	/*
	** Returns the properly formatted API request parameters
	** Args:
	**		$params: an array of parameters
	** More info: http://doc.doyoubuzz.com/pro/auth
	*/ 
	private function buildDoYouBuzzAPIRequestParams( $params )
	{
		$apiSecret 	= "276pXCnQ966PRk3SlOBD-w18_";
		$paramStr 	= "";
		ksort( $params );

		foreach ($params as $key => $val){
			$paramStr .= $val;
		}

		$hash = md5( $paramStr . $apiSecret );
		return array_merge( $params, array("hash" => $hash ) );
	}

	/*
	** Returns a concatenation of the API url + API method + method parameters
	** Args:
	** 		$method: the API method
	**		$params: the parameters ot pass to the method
	*/
	private function buildDoYouBuzzAPIRequest($method, $params)
	{
		$url = "http://showcase.doyoubuzz.com/api/v1/" . $method . "?";

		$idx = -1;
		foreach ($params as $key => $val)
		{
			if (++$idx > 0){
				$url .= "&";
			}
			$url .= $key ."=" . $val;
		}
		return $url;
	}

}