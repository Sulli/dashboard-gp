<?php

namespace DashGP\DashGPBundle\Services;

use Smalot\PdfParser\Parser;
use fpdi\FPDI;

use DashGP\DashGPBundle\Entity\PayformExtract;
use DashGP\DashGPBundle\Entity\MonthlyPayform;
use Doctrine\ORM\EntityManager;

class PdfHelper
{
	private $dirHelper;
	private $eosDb;
	private $parser;
	private $em;

	public function __construct($dirHelper, $eos, EntityManager $em)
	{
		$this->dirHelper 	= $dirHelper;
		$this->eosDb 		= $eos;
		$this->em 		 	= $em;
		$this->parser 	 	= new Parser();
	}

	/*
	** Splits a PDF file into multiple PDF files, one for each person
	** Args:
	**		$file: the source file
	**		$dest: the destination folder of the PDF files that will be created during the process
	** 		$extract: The extract data object
	*/
	public function split( $file, $dest, PayformExtract $extract )
	{
		// Parses the source file
		$pdf 		= $this->parser->parseFile($file);
		// Gets the pages from the parser
		$pages 		= $pdf->getPages();
		// Instantiates a new PDF file using the source file as its template
		$new_pdf 	= $this->newPDF($file);
		$previous 	= null;

		preg_match('#(\d{2}.\d{4})\.pdf$#', $file, $match);
		$date = str_replace('.', '_', $match[1]);

		// Iterates through each document page
		for ($i = 1; $i <= count($pages); ++$i)
		{
			$destFin = "";
			// Split the current page into multiple sections
			$sections = explode("\n", $pages[$i - 1]->getText());
			// Extract the desired data from the content
			$data = $this->extractData($sections);
			if( $data['id'] == -1 ){
				continue;
			}
			//continue;

			// First loop: assigns the current data to $previous
			// Used to check for multiple pages with the same person
			if ($previous == null)
				$previous = $data;

			// If the person on the current page is the same as that of the previous page: add a page to the current 
			if ($previous['id'] == $data['id'])
			{
				$new_pdf->AddPage();
				$new_pdf->useTemplate($new_pdf->importPage($i));
			}
			// Else: save the new PDF file
			else
			{
				$destFin = $this->save($new_pdf, $previous, $dest, $date);
				$new_pdf = $this->newPDF($file);
				--$i;

				$monthPayform = new MonthlyPayform();

			    $monthPayform->setEosId( $data['id'] );
			    $monthPayform->setMonth( $extract->getMonth() );
			    $monthPayform->setYear( $extract->getYear() );
			    $monthPayform->setDateSubmitted( $extract->getDateSubmitted() );
			    $monthPayform->setFilePath( $destFin[0] );
			    $monthPayform->setTitle( $destFin[1] );
			    $monthPayform->setExtension( "pdf" );
			    $monthPayform->setUploadStatus( MonthlyPayform::STATUS_ACTIVE );
			    $monthPayform->setStatus(true);
			    $monthPayform->setCreatedAt( new \Datetime() );
			    $monthPayform->setUpdatedAt( new \Datetime() );
			    $monthPayform->setCreatedBy( $extract->getCreatedBy() );
			    $monthPayform->setExtract( $extract );

			    $this->em->persist( $monthPayform );
			}

			//Create entry into DB
			//$this->em

			$previous = $data;
		}

		// Saves the last document
		$destFinal = $this->save($new_pdf, $previous, $dest, $date);
		$monthPayform = new MonthlyPayform();

	    $monthPayform->setEosId( $data['id'] );
	    $monthPayform->setMonth( $extract->getMonth() );
	    $monthPayform->setYear( $extract->getYear() );
	    $monthPayform->setDateSubmitted( $extract->getDateSubmitted() );
	    $monthPayform->setFilePath( $destFinal[0] );
	    $monthPayform->setTitle( $destFinal[1] );
	    $monthPayform->setExtension( "pdf" );
	    $monthPayform->setStatus( MonthlyPayform::STATUS_ACTIVE );
	    $monthPayform->setCreatedAt( new \Datetime() );
	    $monthPayform->setUpdatedAt( new \Datetime() );
	    $monthPayform->setCreatedBy( $extract->getCreatedBy() );
	    $monthPayform->setExtract( $extract );

	    $this->em->persist( $monthPayform );
	    $this->em->flush();
	}

	/*
	** Returns a new FPDI instance with a source file already set
	** Args:
	**		$file: the complete path to the source file
	*/
	public function newPDF($file)
	{
		$ret = new FPDI();
		$ret->setSourceFile($file);
		return $ret;
	}

	/*
	** Saves a new PDF file
	** Args:
	**		$pdf: the PDF file
	**		$data: $data used to format the filename
	**		$dest: path to the destination folder
	**		$date: a date (mm_YYYY) used in the filename
	*/
	public function save($pdf, $data, $dest, $date)
	{
		$destination = "";
		try
		{
			$dest .= $data['slug'] . "/payforms/";
			$this->dirHelper->createDirectory($dest);
			$fileName 		= "paie_".$date."_".$data['slug']."_".$data['id'];
			$destination 	= $dest . $fileName.".pdf";
			$pdf->Output( $destination, 'F');
		}
		catch (Exception $e)
		{
			echo $e->getMessage();
		}
		return array( $destination, $fileName);
	}

	/*
	** Returns a set of data used to identify the owner of the file
	** Args:
	**		$sections: the content of a PDF page, split into multiple sections
	*/
	private function extractData($sections)
	{
		// Retrieve the line containing the eos id from the page
		$haystack = current(preg_grep('#^Numerateur : \d+$#', $sections));
		// Isolate the eos id
		preg_match("#\d+#", $haystack, $match);
		// Get rid of the leading 0
        if( empty( $match ) )
        {
            $id = -1;
            echo "pdf without numerator\n";
        }
        else{
            $id = (int) $match[0];
        }

		$slug = $this->eosDb->getUserSlug( $id )['slug'];

		return array('id' => $id, 'slug' => $slug);
	}
}