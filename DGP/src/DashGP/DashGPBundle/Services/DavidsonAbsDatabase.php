<?php

namespace DashGP\DashGPBundle\Services;

/*
** Connection to 'davidson_abs' database
*/
class DavidsonAbsDatabase
{
	private $absDb;

    function __construct( $entityManager )
    {
        $this->absDb = $entityManager->getConnection();
    }

	/*
	** Returns eos id
	** Args:
	**		$id: the user's extranet id
	*/
	public function getEosId($id)
	{
		$query 		= "SELECT eos_id FROM eosIdCorrespondance WHERE extranet_id = $id LIMIT 1";
		$stmt 		= $this->absDb->prepare( $query );
		$stmt->execute();
        $result 	= $stmt->fetch();

		return $result['eos_id'];
	}

	/*
	** Returns extranet id
	** Args:
	**		$eosId: the user's eos id
	*/
	public function getExtranetId( $eosId )
	{
		$query 	= "SELECT extranet_id FROM eosIdCorrespondance WHERE eos_id = $eosId LIMIT 1";
		$stmt 	= $this->absDb->prepare( $query );
		$stmt->execute();
        $result = $stmt->fetch();

		return $result['extranet_id'];
	}

	/*
	** Returns the delays of a user
	** Args:
	**		$id: the user's extranet id
	*/
	public function getUserDelays( $id )
	{
		$query 		=  "SELECT date_soumission, date_decompte, annee, nom_consultant, id_user
				   		FROM decompte 
				   		WHERE id_user = $id"; 

        $stmt 		= $this->absDb->prepare( $query );
        $stmt->execute();
        $result 	= $stmt->fetch();

		return $result;
	}

	/*
	** Returns the delays of a manager's consultants
	** Args:
	**		$id: extranet id of the manager
	*/
	public function getConsultantsDelays( $eosId )
	{
		$query 		=  "SELECT date_soumission, date_decompte, annee, nom_consultant, id_user
				  		FROM decompte 
				  		WHERE id_responsable = $eosId 
				  		ORDER BY nom_consultant";

		$stmt 		= $this->absDb->prepare( $query );
		$stmt->execute();
        $results 	= $stmt->fetchAll();

		return $results;
	}

	public function getConsultantsNamesAndIds( $extranetId )
	{
		$query 	=  "SELECT nom_consultant, id_user 
					FROM decompte 
					WHERE id_responsable = $extranetId 
						AND EXISTS (SELECT eos_id 
									FROM eosIdCorrespondance
                        			WHERE extranet_id = decompte.id_user ) 
					GROUP BY id_user 
					ORDER BY nom_consultant";

        $stmt 		= $this->absDb->prepare( $query );
        $stmt->execute();
        $results 	= $stmt->fetchAll();

		return $results;
	}

	public function getConsultantDelays( $idConsultant )
	{
		$query 	=  "SELECT date_soumission, date_decompte, annee
					FROM decompte 
					WHERE id_user = $idConsultant 
					ORDER BY date_soumission DESC;";
		$stmt 		= $this->absDb->prepare( $query );
		$stmt->execute();
        $results 	= $stmt->fetchAll();

		return $results;
	}  
}