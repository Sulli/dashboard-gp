<?php

namespace DashGP\DashGPBundle\Services;


class SignatureReference
{

    public function getSignature(
            $firstName,
            $lastName,
            $title,
            $company,
            $telephone,
            $mobile,
            $telOrMobile,
            $address,
            $city,
            $country,
            $signToUse
            )
    {
        if($signToUse == 1){
            
            if($telOrMobile === "Mobile"){
                $telephone = $mobile;
            }
    $result = '
        <table cellpadding="0" cellspacing="0" style="width:430px;color:white;font-family:arial,sans-serif;background-color:#FFFFFF;">
                <tr>
                    <td colspan="4" bgcolor="#38b99a" style="vertical-align: bottom;font-size:15px;height:25px;min-width:200px;padding-left:5px;font-weight:bold" nowrap>
                        <span id="sign_sign1_firstName">'.$firstName.'</span><span> </span><span id="sign_sign1_lastName">'.$lastName.'</span>
                    </td>
                    <td  rowspan="7" style="text-align: right;" bgcolor="#38b99a">
                        <img alt="" style="width:149px;vertical-align:bottom;" src="https://extranet.davidson.fr/imagesSignature/h2.png"/>
                             </td>
                    <td rowspan="7">
                        <a target="_blank" href="http://www.davidson.fr/">
                            <img alt="www.davidson.fr" style="width:136px;" src="https://extranet.davidson.fr/imagesSignature/logo+20px.png"/>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" bgcolor="#38b99a" style="padding-left:5px;font-size:11px;font-style: italic;" nowrap>
                        <span style="color:white !important;text-decoration:none !important;"><font color="#FFFFFF" style="text-decoration:none !important;"><span id="sign_sign1_title">'.$title.'</span></font></span>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" bgcolor="#38b99a" style="vertical-align: top;height:25px;padding-left:5px;font-size:11px;" nowrap>
                        <span style="color:white !important;text-decoration:none !important;"><font color="#FFFFFF" style="text-decoration:none !important;"><span id="sign_sign1_company">'.$company.'</span></font></span>
                    </td>
                </tr>

                <tr>
                    <td colspan="4" bgcolor="#38b99a" style="padding-left:5px;font-size:11px;text-decoration:none !important;" nowrap>
                        <a href="tel:'.$telephone.'" style="text-decoration:none !important;" >
                            <span style="color:white !important;"><font color="#FFFFFF" style="text-decoration:none !important;">'.$telOrMobile.' : </font></span>
                            <span style="color:white !important;"><font color="#FFFFFF" style="text-decoration:none !important;"><span id="sign_sign1_telephone">'.$telephone.'</span></font></span>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" bgcolor="#38b99a" style="padding-left:5px;font-size:11px;" nowrap>
                        <span style="color:white !important;text-decoration:none !important;"><font color="#FFFFFF" style="text-decoration:none !important;"><span id="sign_sign1_address">'.$address.'</span></font></span>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" bgcolor="#38b99a" style="vertical-align: top;padding-left:5px;font-size:11px;" nowrap>
                        <span style="color:white !important;text-decoration:none !important;"><font color="#FFFFFF" style="text-decoration:none !important;"><span id="sign_sign1_city">'.$city.'</span></font></span>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" bgcolor="#38b99a" style="vertical-align: top;height:20px;padding-left:5px;font-size:11px;" nowrap>
                        <span style="color:white !important;text-decoration:none !important;"><font color="#FFFFFF" style="text-decoration:none !important;"><span id="sign_sign1_country">'.$country.'</span></font></span>
                    </td>
                </tr>
                <tr>
                    <td style="width:200px;">
                        <a target="-blank" href="http://www.davidson.fr/davidson/gptw/">
                            <img alt="" style="vertical-align: middle;" src="https://extranet.davidson.fr/imagesSignature/bg.png"/>
                        </a>
                    </td>
                    <td width="10">
                        <a target="_blank" href="http://www.viadeo.com/v/company/davidson-consulting?ga_from=Fu:/general;Fb:quick-search-bar;Fe:company">
                            <img alt="" style="vertical-align: middle;text-align:center;" src="https://extranet.davidson.fr/imagesSignature/btn1-02.png"/>
                        </a>
                    </td>
                    <td width="10">
                        <a target="_blank" href="http://www.facebook.com/Davidson.Consulting?ref=hl">
                            <img alt="" style="vertical-align: middle;text-align:center;" src="https://extranet.davidson.fr/imagesSignature/btn2-02.png"/>
                                 </td>
                    <td width="10">
                        <a target="_blank" href="http://www.linkedin.com/company/davidson-consulting?trk=tyah&trkInfo=tarId%3A1412581795508%2Ctas%3Adavidson%2Cidx%3A2-2-7">
                            <img alt="" style="vertical-align: middle;text-align:center;" src="https://extranet.davidson.fr/imagesSignature/btn3-02.png"/>
                        </a>
                    </td>
                    <td style="text-align: right;">
                        <img alt="" style="width:38px;vertical-align:bottom;" src="https://extranet.davidson.fr/imagesSignature/b.png"/>
                    </td>
                    <td bgcolor="black" valign="middle" width="290" style="text-align:center;font-family:arial,sans-serif;font-size:12px;vertical:align;text-decoration:none !important;">
                        <a href="http://www.davidson.fr" style="text-decoration:none !important;" >
                            <span style="color:#a5143b !important;"><font color="#a5143b" style="text-decoration:none !important;">www.davidson.fr</font></span>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td colspan="6" align="center" style="vertical-align: bottom;height:15px;color:black;font-family:Arial,sans-serif;font-style: italic;font-size:7px;">Ensemble adoptons des gestes responsables : n\'imprimez ce mail que si necessaire.</td>
                </tr>
            </table>';
        }
        
        elseif($signToUse == 2){
            $result = '
            <table cellpadding="0" cellspacing="0" style="width:430px;color:white;font-family:arial,sans-serif;background-color:#FFFFFF;">
                <tr>
                    <td colspan="4" bgcolor="#38b99a" style="vertical-align: bottom;font-size:15px;height:25px;min-width:200px;padding-left:5px;font-weight:bold" nowrap>
                        <span id="sign_sign2_firstName">'.$firstName.'</span><span> </span><span id="sign_sign2_lastName">'.$lastName.'</span>
                    </td>
                    <td  rowspan="8" style="text-align: right;" bgcolor="#38b99a">
                        <img alt="" style="width:169px;vertical-align:bottom;" src="https://extranet.davidson.fr/imagesSignature/h2.png"/>
                    </td>
                    <td rowspan="8">
                        <a target="_blank" href="http://www.davidson.fr/">
                            <img alt="www.davidson.fr" style="width:154px;" src="https://extranet.davidson.fr/imagesSignature/logo+20px.png"/>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" bgcolor="#38b99a" style="padding-left:5px;font-size:11px;font-style: italic;" nowrap>
                        <span style="color:white !important;text-decoration:none !important;"><font color="#FFFFFF" style="text-decoration:none !important;"><span id="sign_sign2_title">'.$title.'</span></font></span>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" bgcolor="#38b99a" style="vertical-align: top;height:25px;padding-left:5px;font-size:11px;" nowrap>
                        <span style="color:white !important;text-decoration:none !important;"><font color="#FFFFFF" style="text-decoration:none !important;"><span id="sign_sign2_company">'.$company.'</span></font></span>
                    </td>
                </tr>

                <tr>
                    <td colspan="4" bgcolor="#38b99a" style="padding-left:5px;font-size:11px;text-decoration:none !important;" nowrap>
                        <a href="tel:'.$telephone.'" style="text-decoration:none !important;" >
                            <span style="color:white !important;"><font color="#FFFFFF" style="text-decoration:none !important;">Tel : </font></span>
                            <span style="color:white !important;"><font color="#FFFFFF" style="text-decoration:none !important;"><span id="sign_sign2_telephone">'.$telephone.'</span></font></span>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" bgcolor="#38b99a" style="padding-left:5px;font-size:11px;text-decoration:none !important;" nowrap>
                        <a href="tel:'.$mobile.'" style="text-decoration:none !important;" >
                            <span style="color:white !important;"><font color="#FFFFFF" style="text-decoration:none !important;">Mobile : </font></span>
                            <span style="color:white !important;"><font color="#FFFFFF" style="text-decoration:none !important;"><span id="sign_sign2_mobile">'.$mobile.'</span></font></span>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" bgcolor="#38b99a" style="padding-left:5px;font-size:11px;" nowrap>
                        <span style="color:white !important;text-decoration:none !important;"><font color="#FFFFFF" style="text-decoration:none !important;"><span id="sign_sign2_address">'.$address.'</span></font></span>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" bgcolor="#38b99a" style="vertical-align: top;padding-left:5px;font-size:11px;" nowrap>
                        <span style="color:white !important;text-decoration:none !important;"><font color="#FFFFFF" style="text-decoration:none !important;"><span id="sign_sign2_city">'.$city.'</span></font></span>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" bgcolor="#38b99a" style="vertical-align: top;height:20px;padding-left:5px;font-size:11px;" nowrap>
                        <span style="color:white !important;text-decoration:none !important;"><font color="#FFFFFF" style="text-decoration:none !important;"><span id="sign_sign2_country">'.$country.'</span></font></span>
                    </td>
                </tr>
                <tr>
                    <td style="width:200px;">
                        <a target="-blank" href="http://www.davidson.fr/davidson/gptw/">
                            <img alt="" style="vertical-align: middle;" src="https://extranet.davidson.fr/imagesSignature/bg.png"/>
                        </a>
                    </td>
                    <td width="10">
                        <a target="_blank" href="http://www.viadeo.com/v/company/davidson-consulting?ga_from=Fu:/general;Fb:quick-search-bar;Fe:company">
                            <img alt="" style="vertical-align: middle;text-align:center;" src="https://extranet.davidson.fr/imagesSignature/btn1-02.png"/>
                            </a>
                    </td>
                    <td width="10">
                        <a target="_blank" href="http://www.facebook.com/Davidson.Consulting?ref=hl">
                            <img alt="" style="vertical-align: middle;text-align:center;" src="https://extranet.davidson.fr/imagesSignature/btn2-02.png"/>
                        </a>
                    </td>
                    <td width="10">
                        <a target="_blank" href="http://www.linkedin.com/company/davidson-consulting?trk=tyah&trkInfo=tarId%3A1412581795508%2Ctas%3Adavidson%2Cidx%3A2-2-7">
                            <img alt="" style="vertical-align: middle;text-align:center;" src="https://extranet.davidson.fr/imagesSignature/btn3-02.png"/>
                        </a>
                    </td>
                    <td style="text-align: right;">
                        <img alt="" style="width:38px;vertical-align:bottom;" src="https://extranet.davidson.fr/imagesSignature/b.png"/>
                    </td>
                    <td bgcolor="black" valign="middle" width="290" style="text-align:center;font-family:arial,sans-serif;font-size:12px;vertical:align;text-decoration:none !important;">
                        <a href="http://www.davidson.fr" style="text-decoration:none !important;" >
                            <span style="color:#a5143b !important;"><font color="#a5143b" style="text-decoration:none !important;">www.davidson.fr</font></span>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td colspan="6" align="center" style="vertical-align: bottom;height:15px;color:black;font-family:Arial,sans-serif;font-style: italic;font-size:7px;">Ensemble adoptons des gestes responsables : n\'imprimez ce mail que si necessaire.</td>
                </tr>
            </table>';
        }
        return $result;
    }

	
}