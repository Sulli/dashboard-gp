<?php

namespace DashGP\DashGPBundle\Services;

/*
** Connection to 'eos' database
*/
class EosDatabase
{
	private $eosDb;

    function __construct( $entityManager )
    {
        $this->eosDb = $entityManager->getConnection();
    }

	/*
	** Returns a user's basic information (first name, last name)
	** Args:
	**		$id: the user's eos id
	*/
	public function getUser( $id )
	{
		$query 	=  "SELECT p.first_name, p.last_name , u.email
					FROM user_person AS p
					LEFT JOIN user AS u ON u.id = p.id
					WHERE p.id = $id";

		$stmt 	= $this->eosDb->prepare( $query );
		$stmt->execute();
        $result = $stmt->fetch();

		return $result;
	}

	/*
	** Returns a user's complete information
	** Args:
	**		$id: the user's eos id
	*/
	public function getUserInfos($id)
	{
		$infos = array();
		$gpId  = array();
                        
		$infos = $this->getUserPersonInfos( $id );

		if( !$infos || !$infos["profile_id"] ){
			return null;
		}

		$gpId 			= $this->getGpId( $id, $infos["profile_id"] );
		$salary 		= array( "salary" 		=> $this->getUserSalary( $id ));
		$balances 		= array( "balances" 	=> $this->getUserBalances( $id ));
		$contractDates 	= array( "contract"  	=> $this->getContractDates( $id ));
		$evaluations 	= array( "evaluations" 	=> $this->getUserEvaluations( $id ));

		return array_merge( $infos, $gpId, $salary, $balances, $contractDates, $evaluations );
	}

	/*
	** Returns a user's salary information (amounts and their establishment dates)
	** Args:
	** 		$id: the user's eos id
	*/
	public function getUserSalary($id)
	{
		$query 	=  "SELECT value AS 'amount', DATE_FORMAT(establishment_date, '%d/%m/%Y') AS 'dateFrom'
					FROM history_monthly_pay
					WHERE person_id = $id
					ORDER BY establishment_date ASC";

		$stmt 		= $this->eosDb->prepare( $query );
		$stmt->execute();
        $results 	= $stmt->fetchAll();

		return $results;
	}

	/*
	** Returns a user's yearly balance files information
	** Args:
	**		$id: the user's eos id
	*/
	public function getUserBalances($id)
	{
		$query 	=  "SELECT year, file_title, file_path, file_extension, file_mime_type
					FROM history_yearly_balance
					WHERE person_id = $id
					ORDER BY year ASC";

		$stmt 		= $this->eosDb->prepare( $query );
		$stmt->execute();
        $results 	= $stmt->fetchAll();

		return $results;
	}

	/*
	** Returns a user's mission evaluation files information
	** Args:
	**		$id: the user's eos id
	*/
	public function getUserEvaluations($id)
	{
		$query 	=  "SELECT eval.file_title, eval.file_path, eval.file_extension, eval.file_mime_type, mission.name
					FROM mission_evaluation eval JOIN mission mission
					ON eval.mission_id = mission.id
					WHERE consultant_id = $id";

		$stmt 		= $this->eosDb->prepare( $query );
		$stmt->execute();
        $results 	= $stmt->fetchAll();

		return $results;
	}

	/*
	** Returns a user's mission information
	** Args:
	**		$id: the user's eos id
	*/
	public function getMission($id)
	{
		$query 	=  "SELECT mission.client_id,
						   mission.name,
						   mission.address,
						   mission.client_contact,
						   mission.transport_fee,
						   mission.transport_fee_comment,
						   mission.restoration_fee,
						   mission.restoration_fee_comment,
						   client.name AS client_name,
						   mission.end_date
					FROM mission AS mission 
					JOIN user_client AS client ON mission.client_id = client.id 
					WHERE mission.worker_id = $id
					ORDER BY mission.id DESC";

		$stmt 	= $this->eosDb->prepare( $query );
		$stmt->execute();
        $result = $stmt->fetch();

		return $result;
	}

	/*
	** Returns a user's profile id
	** Args:
	** 		$id: the user's eos id
	*/
	public function getProfileId( $id )
	{
		$query = "SELECT profile_id FROM user_person WHERE id = $id";

		$stmt 	= $this->eosDb->prepare( $query );
		$stmt->execute();
        $result = $stmt->fetch();

		return $result;
	}

	/*
	** Returns a user's information (first name, last name, profile id, manager id, slug)
	** Args:
	**		$id: the user's eos id
	*/
	public function getUserPersonInfos( $id )
	{
		$query 	=  "SELECT 	p.first_name, 
							p.last_name, 
							p.profile_id, 
							p.manager_id, 
							p.slug , 
							p.phone_number, 
							u.email
					FROM user_person AS p
					LEFT JOIN user AS u ON p.id = u.id
					WHERE p.id = $id";
                
		$stmt 	= $this->eosDb->prepare( $query );
		$stmt->execute();
        $result = $stmt->fetch();

		return $result;
	}

	/*
	** Returns a user's gp id
	** Args:
	** Args:
	**		$id: the user's eos id
	**		$type: the profile id of the user
	*/
	public function getGpId( $id, $type )
	{
		$query = "SELECT %field% AS gp_id FROM %table% WHERE id = $id";

		if( $type == "str" )
		{
			// If profile id is 'str': field = adv_id, table = user_subcontractor
			$query = preg_replace('#%field%#', "adv_id", $query);
			$query = preg_replace('#%table%#', "user_subcontractor", $query);
		}elseif ( $type == "cslt" ) {
			// If the profile is consultant, table = user_consultant
			$query = preg_replace('#%field%#', "gp_id", $query);
			$query = preg_replace('#%table%#', "user_consultant", $query);
		}else {
			// Else: field = gp_id, table = user_staff
			$query = preg_replace('#%field%#', "gp_id", $query);
			$query = preg_replace('#%table%#', "user_staff", $query);
		}

		$stmt 	= $this->eosDb->prepare( $query );
		$stmt->execute();
        $result = $stmt->fetch();

		return $result;
	}

	/*
	** Return's a user's contract start + end dates
	** Args:
	**		$id: the user's eos id
	*/
	public function getContractDates( $id )
	{
		$query 	=  "SELECT start_date AS start, end_date AS end
					FROM history_contract
					WHERE person_id = $id";

		$stmt 	= $this->eosDb->prepare( $query );
		$stmt->execute();
        $result = $stmt->fetch();

		return $result;
	}

	/*
	** Returns a user's slug
	** Args:
	** 		$id: the user's eos id
	*/
	public function getUserSlug( $id )
	{
		$query = "SELECT slug FROM user_person WHERE id = $id";

		$stmt 	= $this->eosDb->prepare( $query );
		$stmt->execute();
        $result = $stmt->fetch();

		return $result;
	}

	/*
	** Returns a user's contract start date
	** Args:
	** 		$eosId: the user's eos id
	*/
    public function getStartEndDate( $eosId )
	{           
		$query  =  "SELECT start_date, end_date
					FROM history_contract 
					WHERE person_id = $eosId";

		$stmt 	= $this->eosDb->prepare( $query );
		$stmt->execute();
        $result = $stmt->fetch();

		return $result;
	}

	/*
	** Returns the branches
	** Args:
	** 		$eosId: the user's eos id
	*/
	public function getBranch( $eosId )
	{
		$query 	=  "SELECT br.id, br.name, br.address, br.address_city, br.address_postalcode, br.address_country
		            FROM history_contract AS hc
		            RIGHT JOIN branch AS br ON history_contract.branch_id = br.id
		            WHERE person_id = $eosId";
		
		$stmt 		= $this->eosDb->prepare( $query );
		$stmt->execute();
        $results 	= $stmt->fetchAll();

		return $results;
	}
}