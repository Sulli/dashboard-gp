<?php

namespace DashGP\DashGPBundle\Services;

/*
** Connection to 'extranet' database
*/
class ExtranetDatabase
{
	private $extrDb;

    function __construct( $entityManager )
    {
        $this->extrDb = $entityManager->getConnection();
    }

	/*
	** Returns a user's extranet information (gsm, phone, job title, avatar, email)
	** Args:
	**		$id: the user's extranet id
	*/
	public function getInfos( $id )
	{
		if ( $id === NULL ){
			return array();
		}
	
		$query  =  "SELECT com.cb_gsm, com.cb_telpro, com.cb_job, com.avatar, users.email
					FROM jos_comprofiler com 
					LEFT JOIN jos_users users ON com.id = users.id 
					WHERE com.id = $id";

		$stmt 	= $this->extrDb->prepare( $query );
        $stmt->execute();

		$infos = $stmt->fetch();

		// If cb_telpro exists: format cb_telpro
		if ( isset( $infos["cb_telpro"] ) ){
			$infos["cb_telpro"] = $this->formatPhoneNumber($infos["cb_telpro"]);
		}else{
			// Else: set cb_telpro to empty
			$infos["cb_telpro"] = "";
		}

		// If cb_telpro exists: format cb_gsm
		if ( isset( $infos["cb_gsm"] ) ){
			$infos["cb_gsm"] = $this->formatPhoneNumber($infos["cb_gsm"]);
		}else{
			// Else: set cb_gsm to empty
			$infos["cb_gsm"] = "";
		}
                
		if ( !isset( $infos["cb_telpro"] ) ){
			$infos["cb_telpro"] = "";
		}

		if ( !isset( $infos["cb_job"] ) ){
			$infos["cb_job"] = "";
		}

		if ( !isset( $infos["avatar"] ) ){
			$infos["avatar"] = "";
		}

		if ( !isset( $infos["email"] ) ){
			$infos["email"] = "";
		}

		return $infos;
	}

	/*
	** Returns of properly formatted phone number
	** Args:
	**		$number: the phone number
	*/
	private function formatPhoneNumber($number)
	{
		$tmp = implode(preg_grep("#^[0-9]+$#", str_split($number)));

		if (preg_match("#^[0-9]{10}$#", $tmp))
			return $tmp;

		return "";
	}
}