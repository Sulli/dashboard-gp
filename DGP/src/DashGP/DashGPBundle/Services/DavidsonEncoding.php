<?php

namespace DashGP\DashGPBundle\Services;

class DavidsonEncoding
{

	public function __construct()
	{

	}

	/*
	** Decodes the user id
	** Args:
	**		$encId: encoded id
	*/
	public function decode( $encId )
    {
        try {
        	$decIdUser  = trim( $encId );
            $r1         = substr( $decIdUser , 0, 3 );

            $r2         = substr( $decIdUser , strlen($decIdUser)-2 , 2 );

            if( !is_numeric( $r1 ) or !is_numeric( $r2 ) ) {
                return null;
            }
            if ( $r2 == 0 ){
            	return null;
            }

            $num 	= substr( $decIdUser , 3, strlen($decIdUser)-5 );

            if( $num%$r2 != 0 ){
                return null;
            }

            $davidsonId = ''.( ( $num/$r2 )-$r1 );
            return $davidsonId;
        } catch ( Exception $e ) {
            return null;
        }
    }

    /**
     * Return the encoded ID based on the userID
     * Args : 
     * ** $id : id to encode
     */
    public function encode( $id )
    {
        $r1             = rand(569,915);
        $r2             = rand(55,99);
        $encId          = ( $r1.''.( $id + $r1 ) * $r2).''.$r2;
        return $encId;
    }
}