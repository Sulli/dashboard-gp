<?php

namespace DashGP\DashGPBundle\Services;

use Sinner\Phpseclib\Net\Net_SSH2;

class DocumentHelper
{
	private $dirHelper;

	public function __construct( $dirHelper )
	{
		$this->dirHelper = $dirHelper;
	}

	/*
	** Decompresses then removes a tar.gz
	** Args:
	**		$path: complete path to the archive
	*/
	public function decompress($path)
	{
		$file 		= dirname($path).'/'.basename( $path, '.tar.gz' );
		$archive 	= new \PharData( $file.'.tar.gz' );
		$archive->decompress();

		$tar 		= new \PharData( $file.'.tar' );
		$tar->extractTo( dirname( $path ) );

		unlink( $file.'.tar' );
		unlink( $file.'.tar.gz' );
	}

	/*
	** Moves a collection of files to a new directory
	** Args:
	**		$type: the type of file, also used as the destination subfolder
	**		$src: the source directory where the files are being stored
	**		$dest: the main destination folder
	**		$pattern: a regex pattern used to isolate part of every file's name
	*/
	public function moveFiles($type, $src, $dest, $pattern)
	{
		$src 		= $src.'/'.$type;
		$objects 	= scandir( $src );

		foreach ($objects as $object)
		{
			$match = preg_match($pattern, $object, $result);

			if ( filetype( $src.'/'.$object ) == 'file' && $match === 1 )
			{
				$subdir = $dest.$result[1].'/'.$type;
				$this->dirHelper->createDirectory( $subdir );

				rename( $src.'/'.$object, $subdir.'/'.$object);
			}
		}

		$this->dirHelper->removeDirectory( $src );
	}

	/*
	** Returns the content of a certification
	** Args:
	**		$type: the type of certification (either employer or insurance)
	**		$credentials: the connection information to the remote server (host, login, password)
	**		$uid: the eos id of the user requesting the certification
	*/
	public function getCertification($type, $credentials, $uid)
	{
		$ssh = new Net_SSH2( $credentials['host'] );

		if ( !$ssh->login( $credentials['login'], $credentials['password'] ) ){
			return null;
		}	

		$content = $ssh->exec('echo '.$credentials['password'].' | sudo -S php /var/www/eos/app/console dav:create:'.$type.':certification '.$uid);

		//Finds the first occurence of <?xml of the word document in order to remove the characters before the xml tag 
		$first_occurence 	= stripos( "$content", "<?xml" );
		$content 			= substr( $content, $first_occurence );
                
		return $content;
	}

	/*
	** Returns the content of a file
	** Args:
	**		$path: the complete path to the file
	*/
	public function getDocument($path)
	{
		$handle 	= fopen( $path, 'r' );
		$content 	= fread( $handle, filesize( $path ) );

		fclose( $handle );

		return $content;
	}
}