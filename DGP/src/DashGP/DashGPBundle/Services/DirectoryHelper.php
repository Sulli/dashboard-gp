<?php

namespace DashGP\DashGPBundle\Services;

class DirectoryHelper
{
	/*
	** Creates a directory
	** Args:
	**		$path: the complete path of the directory to create
	*/
	public function createDirectory($path)
	{
		if ($path && !file_exists($path))
		{
			$old_umask = umask(0);
			mkdir($path, 0777, true);
			umask($old_umask);
		}
	}

	/*
	** Removes a directory recursively
	** Args:
	**		$path: the complete path of the directory to remove
	*/
	public function removeDirectory($path)
	{
		if (is_dir($path))
		{
			$objects = scandir($path);

			foreach ($objects as $object)
				if ($object != '.' && $object != '..')
					if (filetype($path . '/' . $object) == 'dir')
						removeDirectory($path . '/' . $object);
					else
						unlink($path . '/' . $object);
			reset($objects);
		}
	}

	/*
	** Adds a '/' to the end of a path
	** Args:
	**		$path: the path to format
	*/
	public function formatPath($path)
	{
		if ($path[strlen($path) - 1] != '/')
			$path .= '/';
		return $path;
	}
}