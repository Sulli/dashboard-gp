<?php

namespace DashGP\DashGPBundle\Services;

class DatabaseManager
{
	private $databases;
	private $managers;

	/*
	** Instantiates the database manager
	** Args:
	**		$davidsonAbs: instance of DavidsonAbsDatabase service
	**		$extranet: instance of ExtranetDatabase service
	**		$eos: instance of EosDatabase service
	**		$app_managers: an array of all the profile ids with 'manager' privileges
	*/
	public function __construct( $davidsonAbs, $extranet, $eos, $app_managers )
	{
		$this->databases = array(
			'davidson_abs' 	=> $davidsonAbs,
			'extranet'		=> $extranet,
			'eos'			=> $eos
			);

		$this->managers = $app_managers;
	}

	/*
	** Returns all information about a user gathered from every database
	** Args:
	**		$id: the user's extranet id
	*/
	public function getUserInfos( $id )
	{
		$infos 			= array();
		$infos['id'] 	= $id;
		$infos['eosId'] = $this->databases['davidson_abs']->getEosId( $id );
                
		// Return null if no eos id matches the extranet id
		if ( $infos['eosId'] == null){
			return null;
		}
                
		$eosData = $this->databases['eos']->getUserInfos( $infos['eosId'] );

		if( !$eosData ){
			return null;
		}
                
		
		if( $eosData['profile_id'] == 'cslt' || $eosData['profile_id'] == 'str' )
		{
			// If profile id is 'cslt' or 'str': get the delays of the user
			$infos['delays'] = $this->databases['davidson_abs']->getUserDelays($id);
		}elseif( in_array($eosData['profile_id'], $this->managers) ) 
		{
			// Else if profile id has 'manager' privileges: get the delays of the user's consultants
			$infos['delays'] = $this->databases['davidson_abs']->getConsultantsDelays($id);
		}else{
			// Else: set delays to null
			$infos['delays'] = null;	
		}
                
		return array_merge( $infos, $eosData );
	}

	/*
	** Returns all information about a manager
	** Args:
	**		$eosId: the manager's eos id
	*/
	public function getManagerInfos($eosId)
	{
		$infos['eosId'] = $eosId;
		$infos['id'] 	= $this->databases['davidson_abs']->getExtranetId( $eosId );
		$eosData 		= $this->databases['eos']->getUser( $eosId);
		$extranetData 	= $this->databases['extranet']->getInfos( $infos['id'] );

		$result = array();

		foreach ( $infos  as $key => $value) {
			$result[$key] = $value;
		}
		
		foreach ( $eosData  as $key => $value) {
			if( array_key_exists( $key, $result ) ){
				continue;
			}

			$result[ $key ] = $value;
		}

		foreach ( $extranetData  as $key => $value) {
			if( array_key_exists( $key, $result ) ){
				continue;
			}

			$result[ $key ] = $value;
		}

		return $result;
	}

	/*
	** Returns all information about a user's mission
	** Args:
	**		$eosId: the user's eosId
	*/
	public function getMissionInfos( $eosId )
	{
		$infos = $this->databases['eos']->getMission( $eosId );
		return $infos;
	}
}